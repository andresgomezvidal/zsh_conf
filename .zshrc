#adapted from https://github.com/robbyrussell/oh-my-zsh/blob/master/templates/zshrc.zsh-template
#https://github.com/robbyrussell/oh-my-zsh/blob/master/LICENSE.txt

export ARRAY_START=1

#see options in http://zsh.sourceforge.net/Doc/Release/Options.html
#more stuff in http://zshwiki.org/
[[ "$TERM" == "xterm" ]] && export TERM=xterm-256color

#set variables system wide, eg PATH
if [[ -s /etc/environment && ! "$environment_lock" = true ]]; then
	. /etc/environment
fi
########### differences with bash commands
HISTFILE=~/.zsh_history

alias fc=' fc'
alias history=' fc -l 1'

alias zle=' zle'
alias bindkey=' bindkey'
alias info_widgets='zle -la'

alias vared=' vared'
alias evar=' vared'
alias setopt=' setopt'

alias load_time=' for ((i=0;i<10;i++)); do time zsh -i -c exit; done'

alias antigen_update_all=' antigen selfupdate && antigen update'
alias zgen_update_all=' zgen selfupdate && zgen update'

what_f () {
	if [ -n "$1" ]; then
		local res="$(type -a $1)"
		if [ "$res" =~ "is a shell function" ]; then
			functions "$1"
		else
			echo "$res"
			res="$(command -v $1)"
			if [ "$res" =~ "_f" ]; then
				res=$(awk -F '=' '{print $2}' <<< "$res")
				res=${res//\'/}
				res=${res//\ /}
				functions "$res"
			fi
		fi
	fi
}
alias what=' what_f'

alias tetris=' autoload -U tetris && tetris; echo "ALT+X and enter tetris"'

cd_f () { if [ -z "$1" ]; then \cd; elif [ -d "$1" ]; then \cd "$1"; else \cd "${1%'/'*}"; fi ; }

zsh_cache=${zsh_cache:=~/.zsh/cache}
########### some conf variables
unsetopt flowcontrol


setopt long_list_jobs

setopt interactivecomments

setopt null_glob
setopt extendedglob
if [[ "$ZSH_VERSION" > 5.2.0 ]] ; then
	setopt GLOBSTARSHORT
fi

setopt SUNKEYBOARDHACK

setopt NO_NOMATCH

setopt autocd cdablevars
setopt autopushd pushdminus pushdsilent pushdtohome pushd_ignore_dups
DIRSTACKSIZE=9

setopt SHARE_HISTORY
setopt inc_append_history
setopt HIST_IGNORE_SPACE
setopt HIST_IGNORE_ALL_DUPS
setopt extended_history
setopt hist_expire_dups_first
setopt hist_verify					#history expansion and reload buffer

#HISTSIZE is the maximum number of lines that are kept in a session
HISTSIZE=5000
#SAVEHIST is the maximum number of lines that are kept in the history file
SAVEHIST=100000

setopt menu_complete

setopt complete_in_word
setopt always_to_end

setopt correct_all

setopt prompt_subst					#substitution in prompt

setopt multios						#implicit tees or cats when multiple redirections are attempted

setopt kshoptionprint				#all options are shown, marking enabled an disabled

setopt shwordsplit

setopt chase_links

alias 1='cd -'
alias 2='cd -2'
alias 3='cd -3'
alias 4='cd -4'
alias 5='cd -5'
alias 6='cd -6'
alias 7='cd -7'
alias 8='cd -8'
alias 9='cd -9'



#As you type, the input character is analyzed and, if it may need quoting, the current word is checked for a URI scheme.  If one is found and the current word is not already in quotes, a backslash is inserted before the input character.
autoload -U url-quote-magic
zle -N self-insert url-quote-magic
#fix https://bugs.debian.org/cgi-bin/bugreport.cgi?bug=802581
autoload -Uz bracketed-paste-magic
zle -N bracketed-paste bracketed-paste-magic



########### completion				(BEFORE plugins)
autoload -Uz compinit && {
	if [[ $( date +'%Y%j' ) > $( date +'%Y%j' -r ~/.zcompdump ) ]]; then		#https://github.com/tardypad/dotfiles
		compinit
	else
		compinit -C
	fi

	zstyle ':completion:*:*:*:*:*' menu select
	zstyle ':completion:*' menu yes select
	zstyle ':completion:*' matcher-list 'm:{a-zA-Z}={A-Za-z}' 'r:|[._-]=* r:|=*' 'l:|=* r:|=*'
	#http://grml.org/zsh/zsh-lovers.html
	zstyle ':completion:*:functions' ignored-patterns '_*'		#Ignore completion functions-widgets:
	zstyle '*' single-ignored show								#Show ignored patterns if single.

	zstyle ':completion:*' verbose true
	zstyle ':completion:*:options' description yes

	zstyle ':completion:*:*:kill:*:processes' list-colors '=(#b) #([0-9]#) ([0-9a-z-]#)*=01;34=0=01'

	#kill tab completion
	if [[ "$OSTYPE" = solaris* ]]; then
		zstyle ':completion:*:*:*:*:processes' command "ps -u $USER -o pid,user,comm"
	else
		zstyle ':completion:*:*:*:*:processes' command "ps -u $USER -o pid,user,comm -w -w"
	fi

	#less like tab scrolling list (instead of typical '...show 50 lines?'
	zstyle ':completion:*' list-prompt   ''
	zstyle ':completion:*' select-prompt ''

	#zstyle ':completion:*' insert-tab false

	zstyle ':completion:*' accept-exact '*(N)'
	zstyle ':completion:*' use-cache 1
	zstyle ':completion:*' cache-path "$zsh_cache"


	export LSCOLORS="EXgxcxdxCxegedabagacad"						#BSD
	export LS_COLORS='di=1;34;1:ln=36:so=32:pi=33:ex=1;32:bd=34;46:cd=34;43:su=30;41:sg=30;46:tw=30;42:ow=30;43'
	zstyle ':completion:*:default' list-colors ${(s.:.)LS_COLORS}


	#remove completions already used
	zstyle ':completion:*' ignore-line true
	zstyle ':completion:*:(ffmpeg|avconv):*' ignore-line false


	#allow tab-completion for hidden files too!
	_comp_options+=(globdots)


	compdef -d nocorrect	#deactivate auto completion for nocorrect (so it will complete based on the next command)


	compdef _dirs df
	compdef _dirs cd_f
	compdef _dirs empty_dir_f
	compdef _dirs rmdd_f
	compdef _dirs tdd_f

	compdef zsh=bash		#meaning completion for eg		zsh script.sh

	compdef lsg_f=ls
	compdef lsa_f=ls
	compdef ll_f=ls
	compdef lf_f=ls

	compdef psg_f=pkill
	compdef wpsg_f=pkill
	compdef psgs_f=pkill
	compdef psgk_f=pkill
	compdef ppgrep_f=pkill
	compdef ps_exists_f=pkill
	compdef ifnps_f=pkill
	compdef iamgone_f=pkill
	compdef killall=pkill

	compdef ka_f=pkill
	compdef kar_f=pkill
	compdef karn_f=pkill
	compdef kr_f=pkill
	compdef krn_f=pkill

	compdef monitor_maps_pname_f=pkill

	_mg_f_completion () { reply=($(man -k .|cut -d ' ' -f1)); }
	compctl -K _mg_f_completion mg_f

	compdef command=which
	#compdef nohup_f=which		#if set overwrites default completion, messing with simple commands
	compdef firejail=which
	compdef command_exists_hash_f=which
	compdef command_exists_type_f=which
	compdef what_f=which

	compdef watchcontext_f=kill
	compdef monitor_maps_pnumber_f=kill
	compdef bak_current_maps_f=kill

	compdef psgp_f=kill
	compdef ntw_ps_f=kill

	export binary_formats="doc|docx|pdf|pyc|o|png|jpg|jpeg|gif|tiff|bmp|bpg|PNG|JPG|JPEG|GIF|TIFF|BMP|BPG"
	export latex_related_formats="acn|acr|alg|aux|bbl|bcf|blg|brf|fdb_latexmk|fls|glg|glo|gls|idx|ilg|ind|ist|lof|lot|nav|out|run.xml|snm|tdo|toc|vrb" #not including .log
	compdef '_files -g "^*.($binary_formats|$latex_related_formats)"' vim														#works with nocorrect
	compdef '_files -g "^*.($binary_formats|$latex_related_formats)"' nvim
	zstyle ':completion:*:*:$EDITOR:*' file-patterns '^*.($binary_formats|$latex_related_formats):source-files' '*:all-files'	#does not work with nocorrect
	zstyle ':completion:*:*:nohup_f:*' file-patterns '^*._empty_:source-files' '*:all-files'

	compdef return_alias_f='alias'
	compdef copy_alias_f='alias'

	compdef ev_f='env'


	compdef '_files -g "*.tex"' mv_latex_tmp_f
	compdef '_files -g "*.tex"' latexmkpp_f
	compdef '_files -g "*.tex"' latexmkpvp_f
	compdef '_files -g "*.pdf"' pdf_manager_f

	#correct alias completion (stop alias expansion until completion)
	#but completion won't get inherited
	#setopt complete_aliases

	#compdef eg=printenv
	#compdef '_files -g "^*.o"' e
	#compdef '_files -g "^*.o"' ee
	#compdef '_files -g "^*.o"' egui
	#compdef '_files -g "^*.o"' st
	#compdef '_files -g "^*.o"' sublime-text
	#compdef '_files -g "*.pdf"' $pdf_manager
	#compdef '_files -g "*.pdf"' pdf_manager
	#compdef '_files -g "*.pdf"' pdf
	#compdef '_files -g "*.pdf"' evince
}


########### load stuff

if [ -z "$zgen" ]; then
	zgen=~/.zgen
fi
if [ -z "$antigen" ]; then
	antigen=~/.antigen
fi
if [ -z "$zmanager" ]; then
	if [ -d "$shell_path/manager/zgen" ]; then
		zmanager=zgen
	elif [ -d "$shell_path/manager/antigen" ]; then
		zmanager=antigen
	fi
fi
if [ -z "$zmanager_path" ]; then
	eval zmanager_path=\$$zmanager
fi

if [ "$zmanager" = "zgen" ]; then
	. "$shell_path/manager"/zgen/zgen.zsh

	if ! zgen saved; then
		zgen load zsh-users/zsh-syntax-highlighting
		zgen load olivierverdier/zsh-git-prompt
		if [ -d /usr/share/zsh/site-functions/ ]; then
			fpath=(/usr/share/zsh/site-functions/ $fpath)
		else
			zgen load zsh-users/zsh-completions src
		fi

		zgen save
	fi

elif [ "$zmanager" = "antigen" ]; then
	. "$shell_path/manager"/antigen/antigen.zsh

	if command_exists_hash_f git ; then
		antigen bundle zsh-users/zsh-syntax-highlighting
		antigen bundle zsh-users/zsh-completions
		antigen bundle olivierverdier/zsh-git-prompt

		antigen apply	# Tell antigen that you're done.
	else
		if [ -d "$antigen"/repos ]; then			#there may not even be this folder

			if [ ! $(find "$antigen"/repos/https-COLON--SLASH--SLASH-github.com-SLASH-zsh-users-SLASH-zsh-syntax-highlighting.git -maxdepth 0 -type d -empty 2>/dev/null) ]; then
				antigen bundle zsh-users/zsh-syntax-highlighting
			fi

			if [ ! $(find "$antigen"/repos/https-COLON--SLASH--SLASH-github.com-SLASH-zsh-users-SLASH-zsh-completions.git -maxdepth 0 -type d -empty 2>/dev/null) ]; then
				antigen bundle zsh-users/zsh-completions
			fi

			if [ ! $(find "$antigen"/repos/https-COLON--SLASH--SLASH-github.com-SLASH-olivierverdier-SLASH-zsh-git-prompt.git -maxdepth 0 -type d -empty 2>/dev/null) ]; then
				antigen bundle olivierverdier/zsh-git-prompt
			fi
		fi
	fi
fi

if [ "$GIT_PROMPT_EXECUTABLE" = "haskell" ]; then
	if [ ! -s "$zmanager_path"/olivierverdier/zsh-git-prompt-master/src/.bin/gitstatus ]; then		#haskell update_current_git_vars fix
		echo "Haskell for git prompt is faster but needs to be compiled and can use around 1.5GB"
		echo -n "Doy you want to use haskell? [y/N]"
		read REPLY
		echo ""		# (optional) move to a new line
		if ! [[ $REPLY =~ ^[Nn]$ ]]; then
			GIT_PROMPT_EXECUTABLE="python"
		else
			cd "$zmanager_path"/olivierverdier/zsh-git-prompt-master
			stack setup
			stack build && stack install
		fi
	fi
fi



#LOAD (SOURCE) ALL FILES
for F in "$shellc_path/functions"*; do
	if [ -s "$F" ]; then
		. "$F"
	fi
done
for F in "$load_path"/*.{zsh,sh}; do
	if [ -s "$F" ]; then
		. "$F"
	fi
done


unset GREP_COLOR #deprecated for GREP_COLORS, something outside of my stuff is defining it

alias d='dirs -v | head -10'


#zmv is a module that allow people to do massive rename, but you need to learn how to use it!
#autoload zmv
#alias zmz='noglob zmv'
#alias zcp='noglob zmv -C'
#alias zln='noglob zmv -L'
#alias zsy='noglob zmv -Ls'



########### binkeys
# mapping:
	#	Ctrl=^
	#	Shift=[
	#	Alt=\e

if ! [ "$keybinding_mode" =~ "vi" ]; then
	bindkey -e											# emacs key bindings
	bindkey "^[[1;2D" backward-word
	bindkey "^[[1;2C" forward-word
else
	WORDCHARS=${WORDCHARS/\/} 							#https://superuser.com/a/1436727
	bindkey -v											#zle vi mode
	bindkey '^[[Z' reverse-menu-complete				#shift tab entering vi mode bug
	bindkey '^a' beginning-of-line
	bindkey '^e' end-of-line
	bindkey '^u' kill-whole-line
	bindkey '^y' yank
	bindkey '\e.' insert-last-word
	bindkey '^[[A' up-line-or-search					#arrow hist
	bindkey '^[[B' down-line-or-search					#arrow hist
	bindkey "^?" backward-delete-char					#backspace
	bindkey "^[^?" backward-kill-word					#alt backspace
	bindkey -M vicmd "^[^?" vi-backward-kill-word		#alt backspace
	bindkey -M vicmd 'k' up-line-or-search
	bindkey -M vicmd 'j' down-line-or-search
	bindkey -M vicmd '^a' beginning-of-line
	bindkey -M vicmd '^e' end-of-line
	bindkey -M vicmd '^u' kill-whole-line
	bindkey -M vicmd '^y' yank
	bindkey -M vicmd 'z' beginning-of-line
	bindkey -M vicmd '\C-x\C-e' edit-command-line
	bindkey -M vicmd 'V' edit-command-line

	black_hole_cut(){
		zle vi-set-buffer _
		zle vi-delete
		zle vi-set-buffer 0
	}
	zle -N black_hole_cut
	bindkey -M vicmd 'D' black_hole_cut

	KEYTIMEOUT=1
fi

bindkey '\ew' kill-region
bindkey '^r' history-incremental-search-backward
bindkey "${terminfo[kcbt]}" reverse-menu-complete
bindkey "${terminfo[khome]}" beginning-of-line
bindkey "${terminfo[kend]}"  end-of-line
bindkey "${terminfo[kdch1]}" delete-char
bindkey "^[m" copy-prev-shell-word
bindkey -s "^[OM" "^M"									#bind Intro key as enter



#http://nuclearsquid.com/writings/edit-long-commands/
autoload edit-command-line
zle -N edit-command-line
bindkey '\C-x\C-e' edit-command-line
#bindkey -M vicmd v edit-command-line



#aliases autoexpand
#http://blog.patshead.com/2012/11/automatically-expaning-zsh-global-aliases---simplified.html
expandalias () {
	#if [[ $LBUFFER =~ ' [A-Z0-9]+$' ]]; then
	if [[ $LBUFFER =~ ' [a-zA-Z0-9_*.:]+$' ]]; then
		#blacklist
		#if ! [[ $LBUFFER =~ '$EDITOR' ]]; then
			zle _expand_alias
			zle expand-word
		#fi
	fi
	zle self-insert
}
zle -N expandalias
bindkey " " expandalias
bindkey "^ " magic-space           		# control-space to bypass completion
bindkey -M isearch " " magic-space 		# normal space during searches



#http://stackoverflow.com/a/4766798
#https://github.com/Osse/dotfiles/blob/master/.zfunctions/rationalise-dot
rationalise-dot () {
	local MATCH dir split
	split=(${(z)LBUFFER})
	if (( $#split > 1 )); then
		dir=$split[-1]
	else
		dir=$split
	fi
	if [[ $LBUFFER =~ '(^|/| |'$'\n''|\||;|&)\.$' ]]; then
		zle self-insert
		LBUFFER+=/
		[[ -e $dir ]] && zle -M $dir(:a:h)
	elif [[ $LBUFFER[-1] == '.' ]]; then
		zle self-insert
		[[ -e $dir ]] && zle -M $dir(:a:h)
	else
		zle self-insert
	fi
}
zle -N rationalise-dot
bindkey . rationalise-dot



zle-line-init(){
	# Your other zle-line-init configuration ...

	# Store the last non-empty aborted line in MY_LINE_ABORTED
	if [[ -n $ZLE_LINE_ABORTED ]]; then
	MY_LINE_ABORTED="$ZLE_LINE_ABORTED"
	fi

	# Restore aborted line on the first undo.
	if [[ -n $MY_LINE_ABORTED ]]; then
		local savebuf="$BUFFER" savecur="$CURSOR"
		BUFFER="$MY_LINE_ABORTED"
		CURSOR="$#BUFFER"
		zle split-undo
		BUFFER="$savebuf" CURSOR="$savecur"
	fi
}
zle -N zle-line-init




########### highlight
ZSH_HIGHLIGHT_HIGHLIGHTERS=(main brackets)

# Override highlighter colors
ZSH_HIGHLIGHT_STYLES[default]=none
ZSH_HIGHLIGHT_STYLES[unknown-token]=fg=009
ZSH_HIGHLIGHT_STYLES[reserved-word]=fg=009,standout
ZSH_HIGHLIGHT_STYLES[alias]=fg=013,bold
ZSH_HIGHLIGHT_STYLES[builtin]=fg=230,bold
ZSH_HIGHLIGHT_STYLES[function]=fg=195,bold
ZSH_HIGHLIGHT_STYLES[command]=fg=015,bold
ZSH_HIGHLIGHT_STYLES[precommand]=fg=231
ZSH_HIGHLIGHT_STYLES[commandseparator]=fg=015,bold
ZSH_HIGHLIGHT_STYLES[hashed-command]=fg=009
ZSH_HIGHLIGHT_STYLES[path]=fg=010,bold
ZSH_HIGHLIGHT_STYLES[path_prefix]=fg=010
ZSH_HIGHLIGHT_STYLES[path_approx]=fg=010,underline
ZSH_HIGHLIGHT_STYLES[globbing]=fg=063
ZSH_HIGHLIGHT_STYLES[history-expansion]=fg=014
ZSH_HIGHLIGHT_STYLES[single-hyphen-option]=fg=075
ZSH_HIGHLIGHT_STYLES[double-hyphen-option]=fg=117
ZSH_HIGHLIGHT_STYLES[back-quoted-argument]=fg=013
ZSH_HIGHLIGHT_STYLES[single-quoted-argument]=fg=154
ZSH_HIGHLIGHT_STYLES[double-quoted-argument]=fg=011
ZSH_HIGHLIGHT_STYLES[dollar-double-quoted-argument]=fg=009
ZSH_HIGHLIGHT_STYLES[back-double-quoted-argument]=fg=009
ZSH_HIGHLIGHT_STYLES[assign]=fg=050,bold
ZSH_HIGHLIGHT_STYLES[comment]=fg=015

#doesn't work with simple redirections like &>, so not useful
#ZSH_HIGHLIGHT_STYLES[redirection]=fg=154,bold,standout
#for some reason is-at-least is not working :(
if [[ "$ZSH_VERSION" > 5.1.1 ]] ; then
	ZSH_HIGHLIGHT_STYLES[suffix-alias]=fg=010,bold,underline
fi

########### last sources

if [ -s "$load_path"/tmux ] ; then
	. "$load_path"/tmux
fi
