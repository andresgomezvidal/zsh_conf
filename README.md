# zsh_conf
My current zsh configuration (zshrc, aliases, theme, functions), still learning a lot everyday.

This is intended for personal use mostly, like practicing using git and GitHub/Lab and share with some friends.

I have lots of references to own folders, some spanish comments and so on, because atm I don't think they matter much but maybe in the future I will do something about it.


For a installation like mine with a one line command:

	cd; url="https://gitlab.com/andresgomezvidal/zsh_conf/raw/master/custom/" && wget "$url"git_start_all.sh && wget "$url"functions_min.sh && chmod a+x git_start_all.sh && ./git_start_all.sh -s

I recommend copying the files instead of symlinks, in case I change/remove functionality or make a mistake.
Else just add the code you are interested in to your conf.

Then open a new terminal or source ~/.zshenv and ~/.zshrc.


.shell\_options is for easy customization. Anything else can be done changing these files or adding in $load\_path a file (.zsh for zsh, .bash for bash and .sh for both).
