if [ "$sourced_personal" = true ]; then
	return 0
fi
sourced_personal=true
personal=${personal:="$load_path"/personal.sh}




alias keychain_evail_ssh=' if [ -s ~/.ssh/keychain_list ]; then cat ~/.ssh/keychain_list |while read -r i; do { eval `keychain --eval --agents ssh $i` } &> /dev/null ; done ; else { eval `keychain --eval --agents ssh id_rsa` } &> /dev/null ; fi'


alias mcomix=' mcomix'


#was kinda stupid to have several sets of aliases for different uses (read,edit) for the same files
#I will keep the most used though
alias ep='e $personal'
alias em='e main.c'
alias eb='e bib.bib'

alias einst='e ~/conf_copy/install_list && replace sort -uf ~/conf_copy/install_list'



Downloads="${Downloads:="$(fall_back_dir_f ~/Downloads ~/Descargas)"}"
scripts="${scripts:="$(fall_back_dir_f ~/scripts ~/.scripts)"}"
data_path="${data_path:="$(fall_back_dir_f /data /data_$USER)"}"
media="${media:="$(fall_back_dir_f /run/media/$USER /media/$USER /run/media/ /media)"}"
trash=${trash:=~/.local/share/Trash/files}
trash2=${trash2:="$data_path"/.Trash/$UID/files}
trash3=${trash3:="$data_path"/.Trash-$UID/files}


templates=${templates:=~/Templates}
template_mainc=${template_mainc:="$templates"/main.c}
template_latex=${template_latex:="$templates"/latex.tex}
template_bib=${template_bib:="$templates"/bib.bib}
template_sh=${template_sh:="$templates"/script.sh}
template_makefile=${template_makefile:="$templates"/Makefile}

lesskeys=${lesskeys:=~/lesskeys}
alias lesskey_load=' lesskey "$lesskeys"'


alias ctemplate_makefile=' cp -i "$template_makefile" .'
alias ctemplate_mainc=' cp -i "$template_mainc" .'
alias ctemplate_sh=' cp -i "$template_sh" .'
alias ctemplate_latex=' cp -i "$template_latex" .'
alias ctemplate_GPL3=' cp -i "$templates"/GPL\ v3 .'
alias ctemplate_GPL2=' cp -i "$templates"/GPL\ v2 .'
alias ctemplate_MIT=' cp -i "$templates"/MIT .'

alias kt=' killall $Telegram|| killall telegram-desktop|| killall telegram-deskto'

alias krt=' pkr $Telegram'
alias krpad=' pkr "$pad_manager"'
alias kvlc=' killall -9 vlc'

alias Shell_link_to_home=' ln -si "$shell_path"/*shrc "$shell_path"/.*env ~'
alias Shell_copy_to_home=' cp -i "$shell_path"/*shrc "$shell_path"/.*env ~'
alias Shell_link_to_custom=' ln -si "$shellc_path"/aliases.sh "$shellc_path"/personal.sh "$shellc_path"/zzz_common_interactive.sh "$shellc_path"/*.bash "$shellc_path"/*.zsh "$load_path"'
alias Shell_copy_to_custom=' cp -i "$shellc_path"/aliases.sh "$shellc_path"/personal.sh "$shellc_path"/zzz_common_interactive.sh "$shellc_path"/*.bash "$shellc_path"/*.zsh "$load_path"'


alias ccopy_history_last=' cut -d ";" -f2 <<< "$(tail -1 "$HISTFILE")"|pbcopy'


alias git_personal_acp_all='Shell_acp ; atk_acp ; atk_private_acp'
alias git_personal_pull_all='Shell_pull; atk_pull; atk_private_pull; dotfiles_pull'
alias git_personal_start_all='$shellc_path/git_start_all.sh'


#better than plugin
alias cdt='cdd "$trash"'
alias cdt2='cdd "$trash2"'
alias cdt3='cdd "$trash3"'
alias cdw='cdd "$Downloads"'
alias cdto='cdd "$Downloads"/Torrents'
alias cds='cdd "$scripts"'
alias cdsc='cdd "$shellc_path"'
alias cdm='cdd "$media/"'
alias cdda='cdd "$data_path"'
alias cddata='cdd "$data_path"'
alias cdD='cdd "$data_path"'
alias cdatk='cd "$atk"/data'
alias cdtemplates='cdd "$templates"'



if [ "$personal_lock" != true ]; then
	tareaspendientes=${tareaspendientes:=~/Dropbox/tareas-pendientes.txt}
	tricks=${tricks:=~/Dropbox/Ingenieria/Otras_cosas/ZZZ_cosas/.zzz_cosas/tricks}
	asignaturas_actuales=${asignaturas_actuales:=~/Dropbox/Ingenieria/asignaturas_actuales/}
	examenes=${examenes:="$asignaturas_actuales"/examenes.txt}
	zzz_cosas=${zzz_cosas:=~/Dropbox/Ingenieria/Otras_cosas/ZZZ_cosas/.zzz_cosas/}

	alias etareas='e "$tareaspendientes"'
	alias etricks='e "$tricks"'
	alias eexamenes='e "$examenes"'

	alias cdcosas='cdd "$zzz_cosas"'
	alias cda='cdd "$asignaturas_actuales"'

	#fall_back_f dotfiles_path "$data_path"/dotfiles ~/dotfiles
	dotfiles_path="${dotfiles_path:="$(fall_back_dir_f "$data_path"/dotfiles ~/dotfiles)"}"
	alias dotfiles_pull=' cd "$dotfiles_path" && git pull && cd "$OLDPWD"'
	alias dotfiles_acp=' cd "$dotfiles_path" && acp && cd "$OLDPWD"'
	alias cddot=' cd "$dotfiles_path"'

	atk=${atk:=~/.config/autokey}
	if [ -d "$atk/.git" ]; then
		autokey=${autokey:=~/.config/autokey}
		atkl=${atkl:="$atk"/autokey.log}

		alias atk_pull=' cd "$atk" && git pull && cd "$OLDPWD"'
		alias atk_acp=' cd "$atk" && acp && cd "$OLDPWD"'

		atk_private=${atk_private:=~/.config/autokey_Private}
		if [ -d "$atk_private/.git" ]; then
			autokey_private=${autokey_private:=~/.config/autokey_Private}
			alias atk_private_pull=' cd "$atk_private" && git pull && cd "$OLDPWD"'
			alias atk_private_acp=' cd "$atk_private" && acp && cd "$OLDPWD"'
		fi
	fi

	ltrim=${ltrim:="$scripts"/trim.log}
	smartl=${smartl:="$scripts"/smartctl.log}
	smartcltl=${smartcltl:="$smartl"}

	re=${rh:="$scripts"/etc.log}
	alias rel='ccat "$re"'
	rh=${rh:="$scripts"/home.log}
	alias rhl='ccat "$rh"'
	rht=${rht:="$scripts"/$(trim_f ${HOME}).log}
	alias rht='ccat "$rht"'
	rdata=${rdata:="$scripts"/data.log}
	alias rdatal='ccat "$rdata"'
fi

alias youtube-dl_best=' youtube-dl -f best'

alias pulseaudio_restart_user='systemctl --user mask  pulseaudio.socket; systemctl --user stop pulseaudio.service; systemctl --user unmask pulseaudio.socket; systemctl --user start pulseaudio.service'

alias vpn_start='nohup $scripts/vpn_res.sh'
alias vpn_stop=' sudo killall openvpn'
alias vpn_on='vpn_unlock; vpn_start'
alias vpn_off='vpn_lock; vpn_stop'
alias vpn_lock=' touch /tmp/vpn_res_block.lock'
alias vpn_unlock=' rm /tmp/vpn_res_block.lock'
alias vpn_restart=' vpn_stop; sleep 2; vpn_on; '
#alias vpn_restart=' vpn_stop; sleep 2; nohup sudo /sbin/openvpn --config /etc/openvpn/fav/fav.ovpn; '
alias cvpn='ls -al /etc/openvpn/fav/fav.ovpn |awk -F "/" '\''{print $NF}'\'' '
alias nvpn='
VPNL="$(ls -al /etc/openvpn/*.ovpn |awk '\''{print $NF}'\'')";
VPNC=$(\grep -n "$(\ls -al /etc/openvpn/fav/fav.ovpn |awk '\''{print $NF}'\'' |awk -F "/" '\''{print $NF}'\'')" <<<"$VPNL" |  cut -d ":" -f1);
VPNC=$((VPNC + 1)); VPNC=$((VPNC % $(wc -l <<< "$VPNL")));
if [ "$VPNC" -eq 0 ]; then VPNC=1; fi
VPNN=$(awk '\''NR=='\''$VPNC <<<"$VPNL");
\ln -svf $VPNN /etc/openvpn/fav/fav.ovpn;
vpn_restart
'
alias vpn_time='
TOTAL_LIST=""
for i in /etc/openvpn/*.ovpn; do
	avg=$(ping -c 3 $(awk '\''$1=="remote" {print $2}'\'' $i) |awk '\''$1=="64" {a=$8; gsub("time=","",a); c+=a}; END{printf "%d", c/3}'\'')
	i="$(awk -F "/" '\''{print $NF}'\'' <<< "$i" )"
	echo -e "$avg $i"
	TOTAL_LIST="$TOTAL_LIST
$avg $i"
done

echo ""
sort -rn <<< "$TOTAL_LIST"
'
alias cvpn_1=' \ln -svf "$(find /etc/openvpn -maxdepth 1 -type f -name "01*.ovpn"|head -1)" /etc/openvpn/fav/fav.ovpn; vpn_restart'

alias feh_lock=' touch /tmp/my-feh-background.lock'
alias feh_unlock=' rm /tmp/my-feh-background.lock'
alias feh_black=' feh_lock; feh_background'
alias feh_background=' nohup $scripts/feh.sh'

alias email_manager_script='nohup "$scripts/email_client.sh"'


if [ "$GUI_lock" != true ]; then
	alias ethunar='e "$thunar"/accels.scm'
	alias ecaja='e "$caja"/accels'
	alias enemo='e ~/.gnome2/accels/nemo'
	alias erc='e "$openbox"/rc.xml'

	redshift_conf=${redshift_conf:=~/.config/redshift.conf}
	openbox=${openbox:=~/.config/openbox}
	thunar=${thunar:=~/.config/Thunar}
	caja=${caja:=~/.config/caja}


    alias brightness_down=" sudo chown andres:andres /sys/class/backlight/intel_backlight/{actual_,}brightness; awk '{print \$1-50}' /sys/class/backlight/intel_backlight/brightness > /sys/class/backlight/intel_backlight/brightness"
    alias brightness_up=" sudo chown andres:andres /sys/class/backlight/intel_backlight/{actual_,}brightness; awk '{print \$1+50}' /sys/class/backlight/intel_backlight/brightness > /sys/class/backlight/intel_backlight/brightness"


	if [ "$GUI_more_lock" != true ]; then
		xrandr_load_f(){
			export_z_f xrandr_resolution $(xrandr 2> /dev/null| awk '$2=="connected" {print $0}' | awk -F "+" '{print $1}' | awk '{print $NF}')
			export_z_f xrandr_output $(xrandr 2> /dev/null| awk '$2=="connected" {print $1}' )
			alias xres_reset=' xrandr --output $xrandr_output --mode $xrandr_resolution'
			alias xrandr_inverted=' xrandr --output $xrandr_output --rotate inverted'
		}
		alias xrandr_load=' xrandr_load_f'

		if [ "$xrandr_lock" != true ]; then
			xrandr_load_f
		fi
		alias ED='export DISPLAY=:0.0'

		alias compress-image_overwrite='mogrify -quality 80'
		alias resize_image='mogrify -resize $xrandr_resolution'

		alias horario='image_manager ~/Dropbox/Ingenieria/horario.png'

		alias iamback='music_manager; autokey; conky; telegram_tray; email_manager_script;'
		alias iamgone=' iamgone_f'
		alias afk=' iamgone -f'
		alias afkm='afk -m'

		alias conky_network='nohup \conky -c $dotfiles_path/.conkyrc_network'
		alias mood_unpaired=' find -type f -name "*.mood" |while read -r i; do name="$(awk -F "/" '\''{print $NF}'\'' <<< "$i")"; name="${name:1}"; name="$(awk -F ".mood" '\''{print $1}'\'' <<< "$name")"; dir="$(before_last_occurrence / $i)"; if [[ -z "$(find "$dir" -type f -name "$name.*")" ]]; then echo "NOT FOUND PAIR $i" ; fi; done'

		iamgone_f () {
			for i in "$@"
			do
				if [[ "$i" =~ [Aa] ]]; then
					local KEEP_AUTOKEY=true
				fi
				if [[ "$i" =~ [Cc] ]]; then
					local KEEP_CONKY=true
				fi
				if [[ "$i" =~ [Dd] ]]; then
					local KEEP_DROPBOX=true
				fi
				if [[ "$i" =~ [Ff] ]]; then
					local KEEP_FILE_MANAGER=true
				fi
				if [[ "$i" =~ [Mm] ]]; then
					local KEEP_MUSIC_MANAGER=true
				fi
				if [[ "$i" =~ [Pp] ]]; then
					local KEEP_PAD=true
				fi
				if [[ "$i" =~ [Tt] ]]; then
					local KEEP_TELEGRAM=true
				fi
				if [[ "$i" =~ [Ee] ]]; then
					local KEEP_EMAIL=true
				fi
				if [[ "$i" =~ [Ww] ]]; then
					local KEEP_WHATSAPP=true
				fi
			done

			if ! [ "$PWD" = '/' ]; then					#to avoid locking a device
				local cdold_iamgone=true
				cd /
			fi

			if ! [ "$KEEP_AUTOKEY" = true ]; then killall -q autokey-gtk; fi;
			if ! [ "$KEEP_MUSIC_MANAGER" = true ]; then if [ "$music_manager" = "exaile" ]; then exaile -s; fi; killall -q $music_manager; fi;
			if ! [ "$KEEP_CONKY" = true ]; then killall -q conky; fi;
			if ! [ "$KEEP_FILE_MANAGER" = true ]; then killall -q $file_manager; fi;
			if ! [ "$KEEP_DROPBOX" = true ]; then if command_exists_type_f kdropbox ; then kdropbox; else killall -q dropbox; fi; fi;
			if ! [ "$KEEP_PAD" = true ]; then killall -q $pad_manager; fi;
			if ! [ "$KEEP_TELEGRAM" = true ]; then kt; fi;
			if ! [ "$KEEP_EMAIL" = true ]; then killall -q $email_manager; fi;
			if ! [ "$KEEP_WHATSAPP" = true ]; then killall -q "$WhatsApp";killall -q "$WhatsApp"; fi;

			if [ -n "$cdold_iamgone" ]; then
				cd "$OLDPWD"
			fi

			#window_id=$(xdotool getactivewindow); wmctrl -k on;
			#xdotool windowmap $window_id;
			xdotool type "iamback";
			$lock_manager;
		}
	fi
fi
