if [ "$sourced_common_interactive" = true ]; then
	return 0
fi
sourced_common_interactive=true
#common file for interactive programs related stuff
common_interactive=${common_interactive:="$load_path"/zzz_common_interactive.sh}
alias ecommon_interactive='e "$common_interactive"'

if command_exists_hash_f fasd ; then
	if ! [ "$fasd_lock" = true ]; then
		eval "$(fasd --init auto)"
		alias fasd_cd=' nocorrect fasd_cd'
		alias fasd=' nocorrect fasd'
		alias v='f -e $EDITOR'
		export fasd=~/.fasd
		alias cf='less $fasd'
		alias zz='fasd_cd -d -i'
		alias fasd_only_existing=' awk -F '\''|'\'' '\''system("test -e \""$1"\" || test -d \""$1"\"")==0 {print $0}'\'' "$fasd" > /tmp/fasd_only_existing
			mv "$fasd" "$fasd".bak_$(date +"%Y_%m_%d-%H_%M_%S") && mv /tmp/fasd_only_existing "$fasd"'
		alias fasd_only_existing_and_unmmounted_media=' awk -F '\''|'\'' '\''function mountString(field){ str=substr(field, index(field,"media")); split(str, arr, "/"); return arr[1] "/" arr[2] "/" arr[3] }
			($1~/\/media\// && system("mount |grep " mountString($1) " &>/dev/null ")!=0) {print $0}
			(system("test -e \""$1"\" || test -d \""$1"\"")==0) {print $0} '\'' "$fasd" > /tmp/fasd_only_existing
			mv "$fasd" "$fasd".bak_$(date +"%Y_%m_%d-%H_%M_%S") && mv /tmp/fasd_only_existing "$fasd"'
	fi
fi

if ! [ "$autoenv_lock" = true ]; then
	source `which activate.sh`
fi

if ! [ "$path_lock" = true ]; then
	if [ -z "$shellc_path" ]; then shellc_path=. ; fi
	if [ -z "$sourced_functions_file" ]; then
		. "$shellc_path/functions_file.sh"
	fi

	appendpath_f "$(ruby -e 'print Gem.user_dir')/bin"
fi
