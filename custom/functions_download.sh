if [ "$sourced_functions_download" = true ]; then
	return 0
fi
sourced_functions_download=true


if [ -z "$shellc_path" ]; then shellc_path=. ; fi

if [ -z "$sourced_functions_string" ]; then
	. "$shellc_path/functions_string.sh"
fi

if [ -z "$youtubedl_history" ]; then
	youtubedl_history=~/.youtube-dl.history
fi


alias wait_f='nohup $shellc_path/wait.sh'

alias yda='nohup $shellc_path/yda.sh'
alias ydap='nohup $shellc_path/yda.sh "$(cpaste)"'

alias ydaw='nohup $shellc_path/wait.sh $shellc_path/yda.sh $(which youtube-dl)'
alias ydawp='nohup $shellc_path/wait.sh $shellc_path/yda.sh $(which youtube-dl) "$(cpaste)"'

alias yd='nohup $shellc_path/yd.sh'
alias ydp='nohup $shellc_path/yd.sh "$(cpaste)"'

alias ydw='nohup $shellc_path/wait.sh $shellc_path/yd.sh $(which youtube-dl)'
alias ydwp='nohup $shellc_path/wait.sh $shellc_path/yd.sh $(which youtube-dl) "$(cpaste)"'


wwget_f () { (\cd ~/Downloads && wget "$1"); }
alias wwget='wwget_f'


ping_check_f(){
    #$1 destino al que hacer el ping
    #$2 [cantidad de paquetes]
    echo "Comprobando conexión con $1 ..."
    local c=3
    if [ -n "$2" ]; then
        c=$2
    fi
    local salida=`ping -c $c "$1" 2>&1`
    if [ -z "$(grep ' 0% packet loss' <<< "$salida")" ]; then
        echo "Se perdieron paquetes:"
        echo "$salida"
        return 1
    fi
}
alias ping_check=' ping_check_f'

get_addr_f(){
    ip addr|awk '$0~/brd/ && $0~/inet [[:digit:]]+.[[:digit:]]+.[[:digit:]]+.[[:digit:]]+/ {print $2}'|awk -F '/' '{print $1}'
}
alias get_addr=' get_addr_f'


check_update_f () {
	local tmp_file=/tmp/check_update_f_wget_file_$2_$(date "+%N")
	wget $1 &> /dev/null -O $tmp_file
	local new="$(md5sum $tmp_file |awk '{print $1}')"
	rm $tmp_file
	if [ -f "$2" ]; then
		if ! [ "$new" = "$(cat $2)" ]; then
			echo "$new" > "$2"
			return 0
		fi
	else
		echo "you must introduce two arguments, \$1: page to be compared and \$2 file of downloaded page"
		if [ -d "$2" ]; then
			echo "$2 already exists and is a directory!!!"
		else
			echo "$new" > "$2"
			return 0
		fi
	fi
	return 1
}
alias check_update=' check_update_f'

do_check_update_f () {
	#$1 command to execute
	#$2 reference and options
	#$3 days ago to check if file was modified
	#$4 dir to store checksums
	#$5 time as in DAYS ago
	#eg:	do_check_update_f "chromium --proxy-auto-detect" "http://gitlab.com/andresgomezvidal" 3 ~/checksums
	local id="$(md5sum <<< $2 |cut -d ' ' -f1)"
	if [[ "$5" =~ 'hour' ]]; then
		time='hour'
	elif [[ "$5" =~ 'minute' ]]; then
		time='minute'
	elif [[ "$5" =~ 'second' ]]; then
		time='second'
	elif [[ "$5" =~ 'month' ]]; then
		time='month'
	elif [[ "$5" =~ 'year' ]]; then
		time='year'
	else
		time='day'
	fi
	if [[ ! -e "$4/$id" || -n "$(find "$4/" -type f -name $id -not -newermt "$3 $time ago")" ]]; then
		if check_update_f "$2" "$4/$id" &> /dev/null ; then
			eval "$1" '$2' &> /dev/null
			return 0
		fi
	fi
	return 1
}
alias do_check_update=' do_check_update_f'

do_check_update_date_f () {
	#$1 command to execute
	#$2 reference and options
	#$3 days ago to check if file was modified
	#$4 dir to store checksums
	#$5 time as in DAYS ago
	#eg:	do_check_update_f "chromium --proxy-auto-detect" "http://gitlab.com/andresgomezvidal" 3 ~/checksums
	local id="$(sha1sum <<< $2 |cut -d ' ' -f1)"
	if [[ "$5" =~ 'hour' ]]; then
		time='hour'
	elif [[ "$5" =~ 'minute' ]]; then
		time='minute'
	elif [[ "$5" =~ 'second' ]]; then
		time='second'
	elif [[ "$5" =~ 'month' ]]; then
		time='month'
	elif [[ "$5" =~ 'year' ]]; then
		time='year'
	else
		time='day'
	fi
	if [[ ! -e "$4/$id" || -n "$(find "$4/" -type f -name $id -not -newermt "$3 $time ago")" ]]; then
		eval "$1" '$2' &> /dev/null
		echo -e "$2\n$3 $time\n$(date)" > "$4/$id"
		return 0
	fi
	return 1
}
alias do_check_update_date=' do_check_update_date_f '

do_check_update_week_f () {
	#$1 command to execute
	#$2 reference and options
	#$3 day of the week
	#$4 dir to store checksums
	#[$5 hour]
	local id="$(sha1sum <<< $2 |cut -d ' ' -f1)"
	if [[ ! -e "$4/$id" || -n `find "$4/" -type f -name $id -not -newermt '1 week ago'` ]]; then
		eval "$1" '$2' &> /dev/null
		echo -e "$2\n$3 $5\n$(date)" > "$4/$id"
		if [ "$(LC_ALL=en_US.UTF-8 date +%A|tr '[A-Z]' '[a-z]')" != "$(tr '[A-Z]' '[a-z]' <<< $3)" ]; then
			touch -d "last $3 $5" "$4/$id"
		else
			touch -d "$5" "$4/$id"
		fi
		return 0
	fi
	return 1
}
alias do_check_update_week=' do_check_update_week_f '

reset_month_checksums_f () {
	local LIMIT
	local i
	local tfiles
	local files
	local file
	local n
	local w
	local d

	cd ~/.zzz_checksums
	LIMIT=30
	tfiles=`find -type f -newermt '1 month ago' -exec grep -l 't=month' {} \;`
	while read -r i; do
		if [ -n "$(awk '$2~/day/ && $1>='$LIMIT' {print $1}' $i)" ]; then
			files+=( "$i" )
		fi
	done <<< "$tfiles"

	n=${#files[@]}
	w=$((n/LIMIT))
	LIMIT=$((LIMIT-1))
	d=0
	for ((i=0;i<$n;i++)); do
		if [[ "$d" -lt "$LIMIT" && $((i%w)) -eq 0 ]]; then d=$((d+1)); fi
		file="${files[$i]}"
		touch -a -m -d "$d days ago" $file
	done

	cd "$OLDPWD"
}
