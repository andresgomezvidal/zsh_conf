if [ "$sourced_aliases" = true ]; then
	return 0
fi
sourced_aliases=true
alias_file=${alias_file:="$load_path"/aliases.sh}

#space at the begining means: don't keep it in history!! (if the option is enabled in .zshrc or .bashrc)
#yeah I keep this compatible with bash, for those rare cases I need to use a bash shell

#alias alias=' alias'	#equivalent to put a blank at the start of all aliases

#When you want to call the command instead of the alias, then you have to escape it and call \neg: to use the original ls you have to write \ls'
alias aalias=' alias'
alias lalias=' alias|less'



#some alias already defined (as alias) but without the blank
alias d=' dirs -v | head -10'




#the exact same but with space
#some are commented because I don't use them much atm, so not used to them so it may be intereseting let them appear in the history
alias w=' w'
alias bc=' bc'
alias bg=' bg'
alias fg=' fg'
alias id=' id'
alias ip=' ip'
alias pg=' pg'
alias ps=' ps'
alias pv=' pv'
alias nl=' nl'
alias tr=' tr'
alias wc=' wc'
alias bye=' bye'
alias cal=' cal'
alias cmp=' cmp'
alias cut=' cut'
alias dig=' dig'
alias env=' env'
alias gdb=' gdb'
alias get=' GET'
alias GET=' GET'
alias ldd=' ldd'
alias lex=' lex'
alias mpc=' mpc'
alias mpv=' mpv'
alias rev=' rev'
alias seq=' seq'
alias set=' set'
alias sql=' sql'
alias tex=' tex'
alias tty=' tty'
alias who=' who'
alias xev=' xev'
alias yes=' yes'
alias zsh=' zsh'
alias bash=' bash'
alias date=' date'
alias echo=' echo'
alias exit=' exit'
alias expr=' expr'
alias eval=' eval'
alias file=' file'
alias head=' head'
alias host=' host'
alias info=' info'
alias ipcs=' ipcs'
alias jobs=' jobs'
alias join=' join'
alias last=' last'
alias lynx=' lynx'
alias more=' more'
alias most=' most'
alias ncdu=' ncdu'
alias pass=' pass'
alias popd=' popd'
alias sift=' sift'
alias stat=' stat'
alias tail=' tail'
alias time=' time'
alias tldr=' tldr'
alias type=' type'
alias uniq=' uniq'
alias yank=' yank'
alias zcat=' zcat'
alias blkid=' blkid'
alias byobu=' byobu'
alias clear=' clear'
alias egrep=' egrep'
alias fgrep=' fgrep'
alias gzcat=' gzcat'
alias iconv=' iconv'
alias kinit=' kinit'
alias klist=' klist'
alias lsblk=' lsblk'
alias lscpu=' lscpu'
alias lsmod=' lsmod'
alias lsusb=' lsusb'
alias mount=' mount'
alias pgrep=' pgrep'
alias print=' print'
alias pushd=' pushd'
alias route=' route'
alias sleep=' sleep'
alias split=' split'
alias touch=' touch'
alias umask=' umask'
alias uname=' uname'
alias watch=' watch'
alias where=' where'
alias which=' which'
alias whois=' whois'
alias zgrep=' zgrep'
alias kdstry=' kdstry'
alias okinit=' okinit'
alias oklist=' oklist'
alias amixer=' amixer'
alias apulse=' apulse'
alias avconv=' avconv'
alias cscope=' cscope'
alias docker=' docker'
alias chatty=' chatty'
alias factor=' factor'
alias figlet=' figlet'
alias howdoi=' howdoi'
alias locale=' locale'
alias locate=' locate'
alias lsattr=' lsattr'
alias octave=' octave'
alias printf=' printf'
alias script=' script'
alias telnet=' telnet'
alias ulimit=' ulimit'
alias uptime=' uptime'
alias screen=' screen'
alias strace=' strace'
alias whatis=' whatis'
alias whence=' whence'
alias wmctrl=' wmctrl'
alias xrandr=' xrandr'
alias apropos=' apropos'
alias crontab=' crontab'
alias fc-list=' fc-list'
alias fortune=' fortune'
alias netstat=' netstat'
alias okdstry=' okdstry'
alias pdfgrep=' pdfgrep'
alias pdfinfo=' pdfinfo'
alias sensors=' sensors'
alias sshpass=' sshpass'
alias strings=' strings'
alias whereis=' whereis'
alias zipinfo=' zipinfo'
alias ifconfig=' ifconfig'
alias iwconfig=' iwconfig'
alias nslookup=' nslookup'
alias printenv=' printenv'
alias texcount=' texcount'
alias alsamixer=' alsamixer'
alias asciiview=' asciiview'
alias typespeed=' typespeed'
alias xbindkeys=' xbindkeys'
alias shellcheck=' shellcheck'
alias screenfetch=' screenfetch'
alias scriptreplay=' scriptreplay'







alias dc='cd'		#I never use the dc calculator
alias cdd=' \cd'	#cdd if you already know it is a dir
alias cd=' cd_f'
alias cdl=' cdl_f'
alias .1='cdd ../'
alias .2='cdd ../..'
alias .3='cdd ../../..'
alias .4='cdd ../../../..'
alias .5='cdd ../../../../..'
alias cd1='cdd ../'
alias cd2='cdd ../..'
alias cdp='cd "$(cpaste)"'
alias cdf='cdd $(dirname $(find . | tail -n 1))'
alias cdpath=' cdpath_f'



#greps
#speed (big projects): ag>grep
#ag:    https://github.com/ggreer/the_silver_searcher

#export_z_f GREP_COLOR '1;37;41'
export_z_f GREP_COLORS '1;37;41'
export_z_f GREP_OPTIONS "--color=always --exclude-dir={.bzr,.cvs,.git,.hg,.svn}"		#deprecated
alias grep=' \grep -iI --color=always --exclude-dir={.bzr,.cvs,.git,.hg,.svn}'

#alias instead of more if else
alias gr='grep -iF'
alias grepc='grep -C 4'
alias grepn='grep --line-number'
alias gp=' grep_pipes_f'
alias grep_pipes=' grep_pipes_f'


if [ "$ag_lock" != true ] && command_exists_hash_f \ag; then
		alias g=' nocorrect \ag -SfF --hidden'
		alias gn='g --numbers'
		export grep=\ag
		alias agg=' \ag'
else
	if ! command_exists_hash_f \ag && "$start_shell_message" != "true" ; then
    	echo "ag not found, falling back to grep..."
	fi
	if command_exists_hash_f grep ; then
		alias g=' nocorrect gr'
		export grep=grep
		alias gn='grepn'
	elif [ "$start_shell_message" != "true" ]; then
		echo -e "\t\tgrep NOT FOUND, YOU ARE F***** UP";
	fi
fi

alias gc='g -C 4'

alias ag=' alias|g'
alias eg='env|g'
alias grep_diff=' diff_grep_f'
alias diff_grep=' diff_grep_f'

alias slg=' slg_f'
syslog=${syslog:=/var/log/syslog}


#man
alias man=' nocorrect \man'
alias m='man'
alias ma='man -a'
#$1:man options, $2:man number, $3:grep options, $4:single dash or none (for grep man arg search), $5:man page, else things to grep
alias mg=' mg_f options page "-iF" none'
alias mgc=' mg_f options page "-iFC 4" none'
alias mga=' mg_f options page "-iF" -'
alias mgac=' mg_f options page "-iFC 4" -'


alias ffmpeg_split=' ffmpeg_split_f'
alias ffmpegs=' ffmpeg_split_f duration '
alias ffmpegse=' ffmpeg_split_f end '
alias ffmpegse_mixed=' ffmpeg_split_mixed_f end '
alias ffmpeg_crf=' ffmpeg_crf_f'
alias ffmpeg_265_crf=' ffmpeg_265_crf_f'

alias top=' nocorrect \top'
alias htop=' nocorrect \htop'
alias iftop=' sudo \iftop'
alias iftopP=' sudo \iftop -P'

alias hg='history |g'


alias ppgrep=' nocorrect ppgrep_f'
alias psgrep=' nocorrect psg_f'
alias psg=' nocorrect psg_f'
alias psgp=' nocorrect psgp_f'
alias psgz=' nocorrect psgz_f'
alias psgs=' nocorrect psgs_f'
alias oldpsg=' /usr/ucb/ps auxww |grep ' #for very old distros


#args or cmd needs to be last to show the full length of the string
alias psa='ps -aux'
export pso_format="uname,pid,ppid,pcpu,pmem,time,start,nlwp,state,args"
alias pso='ps -eo $pso_format'
export pso_format2="uname,pid,ppid,pcpu,pmem,time,start,nlwp,args"
alias pso2='ps -eo $pso_format2'
export pso_format3="uname,pid,ppid,state,args"
alias pso3='ps -eo $pso_format3'
export pso_format4="uname,pid,ppid,pcpu,pmem,time,start,nlwp,tty,psr,args"
alias pso4='ps -eo $pso_format4'
export pso_id_format="uname,pid,ppid,sid,uid,euid,args"
alias pso_id='ps -eo $pso_id_format'

alias psmem='ps -eLf --sort=-rss|less'
alias pscpu='ps -eLf --sort=-pcpu|less'

alias pstree_all='ps awfux |less'

alias pstree=' \pstree'



export_z_f WATCHN '0'$NUMERIC_SEPARATOR'5'
alias watchn='watch -n $WATCHN'
alias wmem='watch "ps -eo uname,pid,ppid,pcpu,pmem,args --sort=-rss"'
alias wcpu='watch "ps -eo uname,pid,ppid,pcpu,pmem,args --sort=-pcpu"'
alias watchpsgrep=' wpsg_f'
alias wpsg=' wpsg_f'
alias psgw=' wpsg_f'
alias watchcontext=' watchcontext_f'
alias wctxt=' watchcontext_f'

alias threadsT='top -H'


alias psd=' \psd'									#profile sync daemon
alias psd_parse='psd p'


alias ipc=' ip addr show'


alias echon='echo -n'
alias echoe='echo -e'


alias ls=' \ls -hF --color=always'
alias sl='ls'
alias lsa='ls -lA'
alias ll='ls -lA'
alias lsaf='lsa_f'
alias lss='lsa -S'
alias lg='lsa|g'
alias l=' ll_f -hFlA --color=always'
alias k='l'
alias ñ='l'
alias lsl='ls -l'
alias lt='lst'
alias lst='ls -lAt'
alias ls1='ls -1'
alias lr='ls -R'
alias lsdot='ls -ld .*'
alias lsh='lsdot'
alias lsg=' lsg_f'
alias lf=' lf_f'
alias ldisku=' l /dev/disk/by-uuid'

if command_exists_hash_f tree ; then
    alias tree=' \tree -a'
    alias tree1='tree -L 1';
else
    alias tree='ls -R | grep ":$" | sed -e "s/:$//" -e "s/[^-][^\/]*\//.\//g" -e "s/^/ /" -e "s/-/|/"';
	if ! [ "$start_shell_message" != "true" ]; then
    	echo "tree not found, using ls"
	fi
fi

alias lsd='tree'

alias named_semaphores='ls -al /dev/shm/sem.*|ccat'


alias lsblk_block-size='lsblk -o NAME,PHY-SeC'


alias du=' \du -ha'
alias du0='du -d 0'
alias du1='du -d 1'
alias dus=' dus_f'
alias du0s=' dus_f -d 0'
alias du1s=' dus_f -d 1'
alias du_type_recursive=' du_type_recursive_f'
alias du_type_recursive_total=' du_type_recursive_total_f'

alias df=' \df -h'

alias free=' \free -h'
#for certain cases where free is a bit less precise
alias free_detailed=' while read line; do
salida="$(echo $line |awk  '\'' {print $2} '\'')";
if [[ $line =~ "MemFree" ]]; then memfree="$salida"; elif [[ -n "$(echo $line | \grep "Active.file." )" ]]; then active="$salida"; elif [[ -n "$(echo $line | \grep "Inactive.file." )" ]]; then inactive="$salida"; elif [[ $line =~ "SReclaimable" ]]; then sreclaimable="$salida"; fi;
done < /proc/meminfo; echo "Total de memoria libre es: $memfree + $active + $inactive + $sreclaimable : $((memfree + active + inactive + sreclaimable)) kB" '

alias is_fstab_mounted='awk '\''$1~!/#/ && $2!="none" && $2!="/" {print $2}'\'' /etc/fstab |while read -r i; do if [ -z "$(mount |awk '\''$3=="'\''$i'\''"'\'')" ]; then  echo "$i IS NOT MOUNTED" ; fi; done'

alias ln='\ln -v'

alias commandv=' nocorrect command -v'
alias cv='commandv'



#checksums
alias cksum=' \cksum'
alias md5sum=' \md5sum'
alias sha1sum=' \sha1sum'
alias sha224sum=' \sha224sum'
alias sha256sum=' \sha256sum'
alias sha384sum=' \sha384sum'
alias sha512sum=' \sha512sum'
alias crc32=' \crc32'

alias dropboxs=' dropbox start'

alias keepassx='nohup keepassx'
alias pidgin='nohup pidgin'
alias remmina='nohup remmina'
alias zeal='nohup zeal'
alias texdoc='nohup texdoc'
alias gmrun='nohup gmrun'
alias playonlinux='nohup playonlinux'
alias dolphin-emu=' nohup dolphin-emu'
alias screenkey='nohup screenkey'
alias pavucontrol='nohup pavucontrol'
alias paman='nohup paman' #can set pulseaudio volumes over 150%
alias simplescreenrecorder='nohup simplescreenrecorder'
#alias steam='nohup steam' 		#problems with nohup


alias info_keymap=' setxkbmap -query |awk '\''END{print $2}'\'' '
alias info_ip=' curl icanhazip.com'	#		Get your current public IP
alias info_ip_json=' curl ipinfo.io'	#		ip, hostname, city, region, country, loc, org
alias info_cpu='less /proc/cpuinfo'
alias info_more=' infobrowser'
alias info_current_shell='ps -p $$|awk '\''END{print $NF}'\'' '
#or		ps -p $$ -oargs=|awk '{print $1}' 		(better if inside script with #!/bin/shell)
alias info_fonts=' fc-list'
alias info_windows='wmctrl -l'
alias info_sockets=' ss -p'
alias info_hardware=' lspci'
alias info_hierarchy='man hier'
alias info_list_disks=' lsblk'
alias info_so='screenfetch'
alias info_mouse_position=' xdotool getmouselocation'
alias info_file_hierarchy='man hier'	#but man hier seems to be out of date :(
alias info_internet_lsof_nice=' lsof -P -i -n'
alias info_io_ports='less /proc/ioports'
alias info_port_range_user=' sysctl net.ipv4.ip_local_port_range'
alias info_services='less /etc/services'
alias info_entropy='cat /proc/sys/kernel/random/entropy_avail'
alias info_colors=' for code in {0..255}; do echo -e "\e[38;05;${code}m $code: Test"; done |less'
alias info_x11-or-wayland=' loginctl show-session `loginctl|grep $USER|awk '\''{print $1}'\''` -p Type'
alias info_pulse='systemctl --user status pulseaudio.service pulseaudio.socket'
alias asciitable='man ascii'
#alias asciitable=' man 7 ascii'
#'man ascii' and 'man 7 ascii' output are the same, 7 refers to the man page, 7 is miscellaneus

alias symlinks=' symlinks -v'

alias wget='\wget -t 99'
alias wgetp=' wget "$(cpaste)"'

alias rllh=' rllh_f'

alias check_commands=' check_commands_f'

alias pv_troll=' while true; do pv -qL 100 *; done'
alias hacker_keyboard=' while true; do pv -qL 100 *; done'


#compression-extraction
alias unp=' \unp'

alias unrar=' \unrar'

alias zip=' \zip'
alias zzip=' zip_f'
alias zzipt=' zip_tex_f'
alias gzip=' \gzip'
alias unzip=' \unzip'
alias unzipt=' unzip -tq'	#test file
alias gunzip=' \gunzip'

alias 7z=' \7z'
alias 7zc=' 7z a'	#+ $1=dest,$2=origin
alias 7ze=' 7za'
alias 7za=' \7za'
alias 7zr=' \7zr'

#tar, create and extract, but you must add next the file name due to the 'f'
alias tar=' \tar'
alias tarc='tar -cpPvzf'
alias tare='tar -pPvxzf'
alias tare2='tar -xjvf'
alias munp=' munp_f'



alias lsof=' \lsof'
#another way: ls -l /proc/<pid>/fd


alias path='echo "$PATH" tr: '

alias ttee=' tee /dev/tty'

alias tac=' \tac'
alias cat=' \cat'
alias less=' less_f'
alias lessf=' less_f'
alias lessd=' \less'
alias ccat=' ccat_f'
alias infol=' infol_f'



alias ecron='crontab -e'
alias lcron='crontab -l'
alias crontab_all=' for user in $(cut -f1 -d: /etc/passwd); do echo "crontab for $user:"; sudo crontab -u $user -l; done'

alias rl=' reload'

alias uuidgen=' \uuidgen || cat /proc/sys/kernel/random/uuid'


#latex
alias pdflatex=' \pdflatex'
alias xelatex=' \xelatex'
alias latexmk=' \latexmk'
alias latexmkp=' latexmk -pdf'
alias latexmkpv=' latexmk -pdf -pvc'
alias lx='latexmk'
alias lxc='latexmk -c'
alias lxp='latexmkp'
alias lxpp=' latexmkpp_f'
alias lxpv='latexmkpv'
alias lxpvp=' latexmkpvp_f'
alias mv_latex=' mv_latex_tmp_f'
alias mv_tex=' mv_latex_tmp_f'
alias latex_mv=' mv_latex'
alias tex_mv=' mv_latex'
export -a latex_related_extensions
latex_related_extensions=("acn" "acr" "alg" "aux" "bbl" "bcf" "blg" "brf" "fdb_latexmk" "fls" "glg" "glo" "gls" "idx" "ilg" "ind" "ist" "lof" "lot" "log" "nav" "out" "run.xml" "snm" "tdo" "toc" "vrb")


alias find_videos_sort_size_length=' find_videos_sort_size_length_f'
alias find_videos_sort_length=' find_videos_sort_length_f'
alias video_duration_seconds=' video_duration_seconds_f'
export -a video_extensions
video_extensions=("3g2" "3gp" "amv" "asf" "avi" "f4a" "f4b" "f4p" "f4v" "flv" "gifv" "mkv" "m2v" "m4p" "m4v" "mng" "mov" "mp2" "mp4" "mpe" "mpeg" "mpg" "mpv" "ogv" "qt " "webm" "wmv")


#editors

#vim
alias vim=' nocorrect \vim'
alias vi=' nocorrect \vi'

if [ "$nvim_lock" != true ] && command_exists_hash_f nvim; then
	alias nvim=' nocorrect \nvim'
	export_z_f EDITOR nvim
	export_z_f EDITOR2 vim
	alias nvdiff='nvim -d'
	alias ddiff='nvim -dR'
else
	if ! command_exists_hash_f nvim && "$start_shell_message" != "true" ; then
		echo "nvim not found, trying vim..."
	fi
	if command_exists_hash_f vim ; then
 		alias nvim='vim'
		export_z_f EDITOR vim
		export_z_f EDITOR2 vi
		alias ddiff='vim -dR'
    else
		if [ "$start_shell_message" != "true" ]; then
			echo "vim NOT FOUND, TRYING VI";
		fi
 		if command_exists_hash_f vi ; then
			alias nvim='vi'
			export_z_f EDITOR vi
			export_z_f EDITOR2 nano
			alias ddiff='diff -W250'
		elif [ "$start_shell_message" != "true" ]; then
 	        echo "vi NOT FOUND, YOU ARE F***** UP";
 	   fi;
	fi;
fi


alias e=' $EDITOR'
export_z_f e $EDITOR
export_z_f SUDO_EDITOR $EDITOR
export_z_f VISUAL $EDITOR
export_z_f OC_EDITOR $EDITOR
export_z_f KUBE_EDITOR $EDITOR

#the not default editor at the moment:
alias ee=' $EDITOR2'

alias er='e -R'
alias eer='ee -R'

alias se='sudo -e'
alias see=' see_f'

alias elog=' elog_f'
alias etxt=' elog_f'
alias et=' elog_f'
alias etex=' etex_f'

alias evim='e ~/.vimrc'
alias evimc='e ~/.vimrc_common'
alias simple_vim=' \evim'


alias ea='e $alias_file'
alias eh='e $HISTFILE'
alias ecommon='e $common_env'
alias ezsh='e ~/.zshrc'
alias ebash='e ~/.bashrc'
alias eoptions='e ~/.shell_options'


##[vb]undle
alias vundle_install='vim +PluginInstall +qall'
alias vundle_update='vim +PluginUpdate +qall'





if [ "$GUI_lock" != true ]; then
	alias terminator='nohup \terminator -m'
	alias terminator_zsh='terminator -e zsh'
	alias terminator_bash='terminator -e bash'
	alias terminal='nohup $terminal_manager'
	alias gnome-terminal='nohup \gnome-terminal --maximize'
	alias xterm='nohup \xterm -maximized'
	alias urxvt='nohup \urxvt'

	alias openbox=' \openbox'
	alias ropenbox='openbox --reconfigure'
	alias openbox_exit=' 
		echo -n "Exit openbox?: [y/N]"
		read REPLY
		echo ""		# (optional) move to a new line
		if [[ $REPLY =~ ^[Yy]$ ]]; then
			openbox --exit
		fi'
	alias kopenbox='openbox_exit'

	alias autokey='ifnps_second autokey autokey-gtk'
	alias autokey-gtk='ifnps_second autokey autokey-gtk'

	alias editor_gui='st'
	alias egui=' editor_gui'

	alias fm=' file_manager_f'							#go there with file manager

	alias screen_off=' xset dpms force off'		#turn off screen
	alias xset_off=' xset dpms force off'
	alias xset_soff=' xset s off'				#turn off default screensaver
	alias xset_ioff=' xset -dpms'				#turn off default standby, hibernate, ... after n minutes		idle off

	if [ "$GUI_more_lock" != true ]; then
		gimp_f () { nohup gimp "$@" ; disown; }
		alias gimp=' gimp_f'

		alias gedit=' nohup gedit'
		alias gvim=' nohup gvim'

		if command_exists_hash_f subl ; then
			alias subl=' nohup subl'
			alias sublime-text='subl'
		else
			sublime_text_path=${sublime_text_path:=/opt/sublime_text/sublime_text}
			alias sublime-text=' nohup $sublime_text_path'
		fi
		if [[ -z "$editor_gui" ]]; then
			editor_gui=$(fall_back_hash_f subl sublime-text $sublime_text_path gedit kate leafpad kwrite nedit tea gvim xemacs);
		fi
		alias st='sublime-text'


		if [[ -z "$pad_manager" ]]; then
			pad_manager=$(fall_back_hash_f qjoypad antimicro);
		fi
		alias pad_manager=' eval_ifnps $pad_manager'

		alias pad='pad_manager'
		alias kpad=' killall -q $pad_manager'


		if [[ -z "$pdf_manager" ]]; then
			pdf_manager=$(fall_back_hash_f evince mupdf xpdf zathura qpdfview okular);
		fi
		#alias pdf_manager='nohup $pdf_manager'
		alias pdf_manager=' pdf_manager_f'
		alias pdf='pdf_manager'

		alias tint2='nohup tint2; disown'
		alias tint='tint2'
		alias audacity='nohup audacity'
		alias audacityp='audacity "$(cpaste)"'
		alias virtualbox='nohup virtualbox'
		alias antimicro='ifnps antimicro --tray'
		alias clementine='ifnps clementine'
		alias strawberry='ifnps strawberry'
		alias amarok='ifnps amarok'
		alias gmpc='ifnps gmpc'
		alias conky='ifnps conky'
		alias evince=' nohup evince'
		alias libreoffice=' nohup libreoffice'
		alias qjoypad='ifnps qjoypad'
		alias redshift='ifnps redshift'
		alias totem='nohup totem'
		alias unclutter='ifnps unclutter -idle 30'
		alias vlc='nohup vlc'
		alias smplayer='nohup smplayer'
		alias eog='nohup eog'
		alias feh='nohup feh'
		alias exaile='nohup exaile'
		alias caja='nohup caja --no-desktop'
		alias nautilus='nohup nautilus --no-desktop'

		alias livestreamer=' \livestreamer'
		alias ttv=' ttv_f'
		alias livestreamer-twitch-gui='nohup livestreamer-twitch-gui'
		alias ttvg='nohup livestreamer-twitch-gui'
	fi
fi




alias ntw_ps=' ntw_ps_f'
alias notify_wait_process=' ntw_ps_f'

alias ev=' ev_f'		#echo variable



alias ncmpcpp=' \ncmpcpp'
alias ncm=' ncmpcpp'




alias return_alias=' return_alias_f'
alias copy_alias=' copy_alias_f'

alias copy_regex_case=' copy_regex_case_f'
alias crc='copy_regex_case'


alias wds=' watch -d -n 1 "df; \ls -FlAt "'


#which ports are open
alias listening=' netstat -tulanp'



alias valg=' valgrind --leak-check=full --show-leak-kinds=all --track-origins=yes -v'

alias makef=' if ! [[ -s "Makefile" || -s "makefile" ]]; then ctemplate_makefile; fi'
alias mk='make'
alias mkc='make clean'
alias mkcr='make clean && make && run'
alias mkcv='make clean && make && rvalg'
alias mkav='make all && rvalg'
alias mka='make all'
alias mkar='make all && run'

alias mctags=' ctags *.c *.h'
alias cclean='make clean; rm -f Makefile tags'

#mkav and mka pretty much are the same, but mka is not generic, but will work if there are several executables and mkv uses valgrind
#mkav and rvalg too, but rvalg only uses valgrind and mkav pre clean && compile
alias run1=' if [ -x "$executable_default" ]; then ./$executable_default; elif [ -x "ejecutable" ]; then ./ejecutable; elif [ -x "ejec" ]; then ./ejec; elif [ -x "main" ]; then ./main; else echo "nothing to run"; fi'
alias rmain=' ./main'
alias rex=' ./$executable_default'
alias rexecutable_default=' ./$executable_default'



alias ses=' setxkbmap es'
alias sus=' setxkbmap us'
alias sw=' if [[ "$(setxkbmap -query |awk '\''END{print $2}'\'')" == "us" ]]; then  setxkbmap es; else setxkbmap us; fi; xmodl'
alias xmodl=' xmod_reset; xmodmap ~/.Xmodmap'
alias xmod_reset=' setxkbmap -layout "$(setxkbmap -query |awk '\''END{print $2}'\'')"'

alias xev_relevant=' xev | awk -F'\''[ )]+'\'' '\''/^KeyPress/ { a[NR+2] } NR in a { printf "%-3s %s\n", $5, $8 }'\'' '		#https://wiki.archlinux.org/index.php/Extra_keyboard_keys

alias xsel=' \xsel'
#original pbcopy is from OSX
alias pbcopy='xsel -l /dev/null --clipboard --input'
#alias pbpaste=' xsel --clipboard --output'
#pbpaste does not seem to be working
#also there is xclip, but it does not seem to be copying to control+ c/v buffer
alias ccopy='tr -d "\n"|pbcopy'
alias cpaste='xsel -bo'
alias pwd=' pwd|correct_spaces_name'
alias cpwd='pwd|tr -d "\n"|pbcopy'
alias pwdc='cpwd'


#date
alias dD=' dc_f dD'
alias dM=' dc_f dM'
alias dS=' dc_f dS'
alias dN=' dc_f dN'
alias date_difference=' date_difference_f'
alias date_difference_var=' date_difference_var_f'





#if you name it mount again you won’t be able to use mount directly when you pass arguments, eg		mount myserver:/share /mnt
alias mountf=' mount |column -t'


alias ping=' \ping'
alias pingg='ping www.google.com'
alias fping='ping -s.2'



alias gpgc=' gpg_c_f'
alias gpgd=' gpg_d_f'







###############
		#end general space zone
###############











#some alias already defined
alias _='nocorrect sudo '
alias ssu=' sudo su'
#whitespace necessary to pass aliases to sudo






#package manager

if command_exists_hash_f pacman ; then

	alias inst='sudo pacman -S --needed'
	alias inst_file='sudo pacman -U'
	alias uninst='sudo pacman -R'
	alias uninst_dep='sudo pacman -Rns'
    alias upgrade='sudo pacman -Syu'
    alias mirror_update='sudo pacman -Syyu'
	alias pkg_search='sudo pacman -SS'
	alias ccache='sudo pacman -Scc'
	alias orphan='sudo pacman -Qdt'
	alias rorphan='sudo pacman -Rns $(pacman -Qdtq)'
	alias verify_dependencies='sudo testdb'
	alias clear_pkg_cache_keep_latest_3='sudo paccache -r'
	alias clear_pkg_cache_uninstalled='sudo paccache -ruk0'

	alias pinfo=' pacman -Qikk'
	alias pinfo_listfiles=' pacman -Qil'
	alias powner=' pacman -Qo'
	alias reverse_dependencies=' pacman -Sii'
	alias list_installed=' pacman -Q'
	alias list_simple=' pacman -Qe|awk '\''{print $1}'\'' '
	alias list_non_local=' pacman -Qqen'
	alias list_aur=' pacman -Qm'

	pkg_manager_log=${pkg_manager_log:=/var/log/pacman.log}
	pkg_mirror_list=${pkg_mirror_list:=/etc/pacman.d/mirrorlist}

	if command_exists_hash_f yay; then
		alias yinst='yay -S'
		alias yuninst='yay -R'
		alias yuninst_dep='yay -Yc'
		alias yupgrade='yay -Syu --devel --timeupdate'
		alias list_installed_aur_yay=' yay -Ps'
	elif command_exists_hash_f yaourt; then
		alias yinst='yaourt -S'
		alias yuninst='yaourt -R'
		alias yuninst_dep='yaourt -Rns'
    	alias yupgrade='yaourt -Syu --aur'
		alias list_installed_aur_yaourt=' yaourt -Qm'
	fi

	powner_exists_f () {
		powner_result="$(sudo pacman -S "$@" 2>/dev/null|tee /dev/tty)"
		pacman -Qo $(echo "$powner_result"|awk '$3=="exists" && $4=="in" && $5=="filesystem" {print $2}') ;
	}
	alias powner_exists=' powner_exists_f'

	no_owner_mv_f () {
		if [[ -n "$1" || -z "$powner_result" ]]; then
			powner_result="$(sudo pacman -S "$@" 2>/dev/null|tee /dev/tty)"
		fi
		local dest=/opt/pacman_no_owner_mv/$1_$(\date "+%Y_%m_%d-%H.%M.%S.%N")
		sudo mkdir -p $dest
		sudo mv -vi $(echo "$powner_result"|awk '$3=="exists" && $4=="in" && $5=="filesystem" {print $2}') $dest ;
	}
	alias no_owner_mv='no_owner_mv_f'

	if [[ -n "$ZSH_VERSION" ]]; then
		_no_owner_mv_f_completion () { reply=($(pacman -Q|cut -d ' ' -f1)); }
		compctl -K _no_owner_mv_f_completion no_owner_mv_f
		compctl -K _no_owner_mv_f_completion powner_exists_f
	fi

elif command_exists_hash_f apt-get ; then

	alias info_pkg='sudo dpkg -s'
	alias aptitude='nocorrect \aptitude'
	alias sag='sudo apt-get'
	alias sagu='sudo apt-get update'
	alias upgrade='sudo apt-get upgrade'
	alias inst='sudo apt-get install'			#specific version like 		sudo apt-get install package=version
	alias uninst='sudo apt-get remove'
	alias rpurge='sudo apt-get remove --purge'
	alias upgrade_only='sudo apt-get install --only-upgrade'
	alias upgrade_only_dry='sudo apt-get install --only-upgrade --dry-run'
	alias upgrade_upgrade_dry='sudo apt-get upgrade --dry-run'
	alias pinfo_cache='sudo apt-get show'
	alias pinfo_dependencies_cache='sudo apt-get showpkg'
	alias stats_cache='sudo apt-get stats'
	alias psource='sudo apt-get --download-only source'
	alias sad='sudo apt-get download'			#download without installing
	alias pkg_search='sudo apt-cache search'
	alias add_repo='sudo apt-add-repository'
	alias remove_repo='sudo ppa-purge'
    alias rm_repo='remove_repo'
	alias depends='sudo apt-rdepends -d'
	alias orphan='sudo apt-get -s autoremove'
	alias rorphan='sudo apt-get autoremove'
	alias ccache='sudo apt-get clean'
	alias verify_dependencies='sudo apt-get check'
	alias satisfy_dependencies='sudo apt-get build-dep'
	alias reverse_dependencies='sudo apt-cache rdepends / aptitude search'
	alias list_installed=' dpkg --get-selections'
    alias list_installed_detailed=' dpkg --list'
	alias list_installed_new=' aptitude search "~N" / aptitude forget-new'
#	alias list_installed_unavailable='sudo deborphan'

	pkg_manager_log=${pkg_manager_log:=/var/log/dpkg.log}

elif command_exists_hash_f dnf;  then
	alias yum='dnf'
	alias inst='sudo dnf install'
	alias uninst='sudo dnf uninstall'
	alias rpurge='sudo dnf remove'
	alias add_repository_pkg_manager='sudo yum-config-manager --add-repo'
	alias pkg_search='sudo dnf search'
	alias ccache='sudo dnf clean all'
	alias rorphan='sudo dnf autoremove'
	alias verify_dependencies='sudo dnf deplist'
	alias satisfy_dependencies='sudo yum-builddep'
	alias reverse_dependencies='sudo dnf resolvedep'
	alias list_installed='sudo dnf list installed'
	alias list_installed_new='sudo dnf list recent'
	alias list_installed_unavailable='sudo dnf list extras'
	alias repoquery_list_files=' dnf repoquery -l'
	alias rmp_list_files_package=' rpm -ql'
	alias rmp_list_installed=' rpm -qa'

	pkg_manager_log=${pkg_manager_log:=/var/log/dnf.log}
elif command_exists_hash_f yum;  then

	alias inst='sudo yum install'
	alias uninst='sudo yum uninstall'
	alias rpurge='sudo yum remove'
	alias add_repository_pkg_manager='sudo yum-config-manager --add-repo'
	alias pkg_search='sudo yum search'
	alias ccache='sudo yum clean all'
	alias rorphan='sudo yum autoremove'
	alias verify_dependencies='sudo yum deplist'
	alias satisfy_dependencies='sudo yum-builddep'
	alias reverse_dependencies='sudo yum resolvedep'
	alias list_installed=' yum list installed'
	alias list_installed_new=' yum list recent'
	alias list_installed_unavailable=' yum list extras'

	pkg_manager_log=${pkg_manager_log:=/var/log/yum.log}

elif [ "$start_shell_message" != "true" ]; then
	echo "NOT FOUND ANY PACKAGE MANAGER (apt-get, dnf, yum or pacman)"
fi



#go
alias go=' go'
alias gob=' go build'
alias gor=' go run'
alias godoc=' godoc'



#python
alias python=' \python'
alias py='python'
alias sage=' \sage'

alias faker=' \faker'
alias fakera='faker name;faker address;faker company'
alias fakerl='faker -l es_ES'
alias fakerla='fakerl name;fakerl address;fakerl company'

pipl=${pipl:=~/.pip/pip.log}

#pip2
alias pip2_inst='pip2 install'
alias pip2_uninst='pip2 uninstall'
alias pip2_sinst='sudo pip2 install'
alias pip2_suninst='sudo pip2 uninstall'
alias pip2_installed=' pip2 freeze'
alias pip2_outdated=' pip2 list --outdated'
										#pip upgrade	http://stackoverflow.com/a/3452888
alias pip2_upgrade_1='pip2 list --outdated 2> /dev/null |awk '\''{print $1}'\'' |xargs -n1 pip2 install -U'
alias pip2_upgrade_2='pip2 freeze --local | grep -v '\''^\-e'\'' | cut -d = -f 1  | xargs -n1 pip2 install -U'
alias pip2_upgrade_sudo='pip2 list --outdated 2> /dev/null |awk '\''{print $1}'\'' |sudo xargs -n1 pip2 install -U'
alias pip2_upgrade_all='sudo pip2 install --upgrade pip; pip2_upgrade_1; pip2_upgrade_sudo'

#pip3
alias pip3_inst='pip3 install'
alias pip3_uninst='pip3 uninstall'
alias pip3_sinst='sudo pip3 install'
alias pip3_suninst='sudo pip3 uninstall'
alias pip3_installed=' pip3 freeze'
alias pip3_outdated=' pip3 list --outdated'

alias pip3_upgrade_1='pip3 list --outdated 2> /dev/null |awk '\''{print $1}'\'' |xargs -n1 pip3 install -U'
alias pip3_upgrade_2='pip3 freeze --local | grep -v '\''^\-e'\'' | cut -d = -f 1  | xargs -n1 pip3 install -U'
alias pip3_upgrade_sudo='pip3 list --outdated 2> /dev/null |awk '\''{print $1}'\'' |sudo xargs -n1 pip3 install -U'
alias pip3_upgrade_all='sudo pip3 install --upgrade pip; pip3_upgrade_1; pip3_upgrade_sudo'



if command_exists_hash_f git ; then
	alias gitlog=' git log -p --stat'
	alias gitc=' git checkout'
	alias gitm=' git merge'
	alias gitmt=' git mergetool'
	alias gitp=' git pull'
	alias git_add_all='git add -A :/ '
	alias git_add-commit='git add -A :/ && git commit'
	alias git_add-commit-push='git add -A :/ && git commit && git push'
	alias git_add-commit-push_master='git add -A :/ && git commit && git push -u origin/master'
	alias acp='git_add-commit-push'
	alias ac='git_add-commit'
	alias gitrao='git remote add origin'
	alias giturl=' git remote show origin'
	alias gitdiff=' git diff'
	alias gitd=' git diff HEAD --stat --patch'
	alias gitdhead=' git diff HEAD --stat --patch'
	alias gitdmaster=' git diff origin/master --stat --patch'
	alias giturl=' git remote show origin'
	alias giturls=' giturl |awk '\''$1=="Fetch" {print $3}'\'' '
	alias giturlsc=' giturl |awk '\''$1=="Fetch" {print $3}'\''|ccopy'
	alias giturl_broken_referential_integrity=' git config --get remote.origin.url'
	alias git_added=' (cd "$(git rev-parse --show-toplevel)"; git ls-files --others --exclude-standard)'
	alias git_head_back_1=' git reset HEAD~1'
	
	alias Shell_pull='if [ -d "$shell_path"/.git ]; then (cd "$shell_path" && git pull); else echo "there is no .git folder present"; fi'
	alias Shell_acp='if [ -d "$shell_path"/.git ]; then (cd "$shell_path" && acp); else echo "there is no .git folder present"; fi'

elif [ "$start_shell_message" != "true" ]; then
	echo "GIT NOT FOUND";
fi


alias ansible-doc=' nocorrect ansible-doc'
alias ansible-galaxy=' nocorrect ansible-galaxy'
#copy the whole ansible_facts output and search for the exact argument to get its path (parents)
ansible_get_json_path_f(){ jq -c 'paths | select(.[-1] == "'$1'")' <<< "$(echo '{'; \xsel -bo |awk 'NR>1 {print $0}')"; }
alias ansible_get_json_path=' ansible_get_json_path_f'


alias search_processes_for_port='sudo lsof -i -P |g'


#make executable, with nice autocompletion, ty zsh!
alias ax='chmod a+x'
alias chmod_not_by_other_users='chmod g-w,o-w'

alias chmod_dir='find -type d -exec chmod 755 {} \;'
alias chmod_file='find -type f -exec chmod 644 {} \;'


alias chattr_invariable='chattr +i'


alias more_entropy='/etc/init.d/rng-tools start'


#if not current dir, cd and go back, kinda a pain to write this else
#zsh autoexpansion ftw
alias fn='fn_f'
alias ff='find -type f '
alias ff1='find -maxdepth 1 -type f '
alias ffod='find -type f -name "*.o" -delete'
alias ffmd='find -type f -name "main" -executable -delete'
alias ffn='ffn_f'
alias ffe='find -type f -empty '
alias ffe1='find -maxdepth 1 -type f -empty '
alias fd='find -type d '
alias fd1='find -maxdepth 1 -type d '
alias fdn='fdn_f'
alias fde='find -type d -empty '
alias fde1='find -maxdepth 1 -type d -empty '
alias fl='find -type l'
alias fl1='find -maxdepth 1 -type l'
alias find_duplicate='find -not -empty -type f -printf "%s\n" | sort -rn | uniq -d | xargs -I{} -n1 find -type f -size {}c -print0 | xargs -0 md5sum | sort | uniq -w32 --all-repeated=separate'
alias find_date='find -printf "%C@ %TY/%Tm/%Td %TH:%TM:%TS %p\n" 2> /dev/null'
alias ffn_date='ffn_date_f'
alias fdn_date='fdn_date_f'
alias find_latest_recursive='find -type f -printf "%C@ %TY/%Tm/%Td %TH:%TM:%TS %p\n" 2> /dev/null |sort -rn |head'
alias find_latest1='find -maxdepth 1 -type f -printf "%C@ %TY/%Tm/%Td %TH:%TM:%TS %p\n" 2> /dev/null |sort -rn |head'
alias find_files_sort_size=' find -type f -exec du -h {} \; |sort -hr'



#stress number of cores, cannot ctrl+c, kill it with 'kill', htop is also a fast way
alias stress1='for i in 1; do while : ; do : ; done & done'
alias stress4='for i in 1 2 3 4; do while : ; do : ; done & done'



export_z_f rrsync 'rsync -azhHX --info=progress2'
alias rrsync='rsync -azhHX --info=progress2'
alias rs='rrsync'
alias srs='sudo rrsync'
alias rsl='rs -L'
alias rse='rrsync --exclude={.bzr,.cvs,.git,.hg,.svn}'

alias cpl='cp --preserve=links'





alias cdiff='colordiff'
alias pdiff='diffpdf'
alias pdfdiff='diffpdf'
alias vdiff='vim -d'
alias fdiff='sdiff -s'
alias wdiff='diff -W250'
alias diffw='diff -W250'
export_z_f ddiff 'vimdiff -R'



alias sshutdown=' if reply_q_f "Shutdown?" "nope"; then sudo shutdown -h now; fi; '
alias sreboot=' if reply_q_f "Reboot?" "nope"; then sudo shutdown -r now; fi; '


alias startup_lock='touch "$startup_lock"'
alias touch_trim_paste=' touch $(trim_f "$(cpaste)")'
alias touch_empty_video=' touch_empty_video_f'


alias systemctl=' \systemctl'
alias sctl='systemctl'
alias sctlu='systemctl --user'

alias journalctl=' \journalctl'
alias jctl='journalctl'
alias jctlu='journalctl --user'
alias jctlun='journalctl --unit'
alias journalctl_errors='journalctl -p 0..3 -xn'

alias boot_time=' if [ -s ~/boot_time.svg ]; then systemd-analyze plot > boot_time.svg; else mbak_f true boot_time.svg; systemd-analyze plot > boot_time.svg; fi'





alias monitor_bak=' "$shellc_path"/monitor_bak.sh'
alias find_monitor=' "$shellc_path"/find_monitor.sh'
alias monitor_maps_pname=' monitor_maps_pname_f'
alias monitor_maps_pnumber=' monitor_maps_pnumber_f'
alias bak_current_maps=' bak_current_maps_f'







alias mkd=' mkdir -p'
alias md=' md_f'



alias cp='cp -vi'
alias rm='rm -v'
alias rmrf='\rm -rf'
alias rmip='rm -vi "$(cpaste)"'
alias rmv='rm -i'
alias rmd='rmd_f'
#but in mv case -i means it will prompt in case of overwrite, so instead of being painful each time, now is helpful sometimes
alias mv='nocorrect mv -i'

alias rmdd='rmdd_f'
alias empty_dir='empty_dir_f'
alias rmt='empty_dir_f "$trash"; empty_dir_f "$trash2"; empty_dir_f "$trash3";'

alias reset_file='reset_file_f rm -f'
alias rm_touch='reset_file_f rm -f'
alias trash_touch=' reset_file_f trash'
alias trash_reset=' reset_file_f trash'
alias bak_reset=' reset_file_f mbak_f false'

alias trash=' \trash'
alias delete='trash'
alias del='trash'
alias t='mtrash'
alias tdd=' tdd_f'
alias mtrash=' trashm_f'
alias mtrash2=' trashm2_f'
alias mtrash3=' trashm3_f'
alias trashm=' trashm_f'
alias trashm2=' trashm2_f'
alias trashm3=' trashm3_f'

alias dut='du -s "$trash"; du -s "$trash2"; du -s "$trash3"'


alias ki='nocorrect kill'
alias ki9='kill -9'
alias k9='kill -9'
alias pk='pkill'

alias pkr=' pkr_f'
alias ka='ka_f'
alias kr=' kr_f'
alias krn=' krn_f'
alias kar=' kar_f'
alias karn=' karn_f'
alias kara=' kar autokey-gtk'


alias mv_ln='mv_ln_f'
alias mv_ln_dot='mv_ln_dot_f'
alias mv_ln_data='mv_ln_data_f'
alias mv_ln_data_config='mv_ln_data_config_f'


alias mpv_link=' mpv_link_f'
mpv_link_f () {
    local url
    if [ -n "$1" ]; then
        url="$1"
    else
        url="$(xsel -bo)"
    fi
    url="$(prev_first_occurrence $url '&')"
    \mpv "$url"
}


#better use builtins, else you can face recursion and other problems
#also I have found it consumes a bit more cpu using aliases, or so it seems using time

cdl_f () { if [ -n "$1" ]; then cd "$1" && l; else "I don't know where to go!"; fi ; }
cdpath_f(){
	local dest
	if [ -n "$1" ]; then
		if [[ "${1:0:1}" == '/' || "${1:0:1}" == '~' ]]; then
			dest="$1"
		else
			dest="$PWD/$1"
		fi
	else
		dest="$PWD"
	fi;
	CDPATH="$CDPATH:$dest";
}

md_f () { \mkdir -p "$1" && \cd "$1"; }
tdd_f () { \cd .. && \trash "$OLDPWD" ; }
empty_dir_f () {
	if [[ "$1" = "." || -z "$1" ]]; then
		\rm -Rf *
		\rm -Rf .*
	else
		if [ -d "$1" ]; then
			\rm -dRf "$1" && \mkdir -p "$1"
		else
			echo "$1 is not a directory";
		fi ;
	fi ;
}
rmd_f () {
	local REPLY
	if [ -n "$1" ]; then
		lsa "$@"
		echo -n "Are you sure you want to remove $@?: [y/N]"
		read REPLY
		echo ""		# (optional) move to a new line
		if [[ $REPLY =~ ^[Yy]$ ]]; then
			\rm -vRf "${@}"
		fi
	else
		echo "Nothing to remove introduced"
	fi ;
}
rmdd_f () {
	local REPLY
	lsa *
	echo -n "Are you sure you want to remove $(current_folder_trim_f)?: [y/N]"
	read REPLY
	echo ""		# (optional) move to a new line
	if [[ $REPLY =~ ^[Yy]$ ]]; then
		\cd ..
		\rm -vRdf "$OLDPWD"
	fi ;
}

see_f(){ local TMP_SUDO_EDITOR=$SUDO_EDITOR; SUDO_EDITOR=$EDITOR2; sudo -e "$@"; SUDO_EDITOR=$TMP_SUDO_EDITOR; }

trashm_f () { if [ -d "$trash" ]; then mv -i "$@" "$trash"; fi ; }
trashm2_f () { if [ -d "$trash2" ]; then mv -i "$@" "$trash2"; fi ; }
trashm3_f () { if [ -d "$trash3" ]; then mv -i "$@" "$trash3"; fi ; }

reset_file_f () { local fn="${@: -1}"; if [ -f "$fn" ]; then "$@" && \touch "$fn"; else echo "$fn is not a file"; fi ; }


pkr_f () { \pkill "$1"||echo "$1 was not running"; eval_nohup_f "$@" ; }
ka_f () { psg_f "$1"; \killall -i "$1" ; }
kr_f () { psg_f "$1"; \killall -i "$1" && eval_nohup_f "$@" ; }
krn_f () { psg_f "$1"; \killall -i "$1" && nohup "$@"; }
kar_f () { \killall "$1"||echo "$1 was not running"; eval_nohup_f "$@"; }
karn_f () { \killall "$1"||echo "$1 was not running"; nohup "$@"; }

slg_f () { grep -iF "$1" "$syslog"; }

#using grep because ag was not working correctly with -C 4 and man
mg_f () {
#$1:man options, $2:man number, $3:grep options, $4:single dash or none (for grep man arg search), $5:man page, else: things to grep
	local gi=
	local mo
	local n
	local go
	local dash
	local i
	if ! [[ "$1" = [Nn][Oo][Nn][Ee] || "$1" = [Nn][Uu][Ll][Ll] || "$1" = [Oo][Pp][Tt][Ii][Oo][Nn][Ss] ]]; then
		mo="$1"
	fi
	case "$2" in
		[Nn][Oo][Nn][Ee] | [Nn][Uu][Ll][Ll] | [Nn][Uu][Mm] | [Pp][Aa][Gg][Ee])
		;;
		*)
			if isnum $2; then
				n=$2
			fi
		;;
	esac
	if ! [[ "$3" = [Nn][Oo][Nn][Ee] || "$3" = [Nn][Uu][Ll][Ll] ]]; then
		go="$3"
	fi
	if ! [[ "$4" = [Nn][Oo][Nn][Ee] || "$4" = [Nn][Uu][Ll][Ll] ]]; then
		dash="-"
	fi

	for i in "${@:6}"; do
		gi="$gi $(\man $mo $n $5 | grep "$go" -- "$dash$i")"
	done;
	ccat <<<"$gi";
}


ppgrep_f () { pid=$(pgrep "$1") && echo "\$pid=$pid"; }
grep_pipes_f () {
	local i
	local last=${@: -1}		#last argument, should be the target file
	local res="$(grep "${@:1:1}" "$last")"
	for i in "${@:2:$# -2}"; do
		res="$(grep "$i" <<< $res)"
	done
	echo "$res";
}

get_options_f(){
	local options
	for i in "$@"; do
		if [ "${i:0:1}" = '-' ]; then
			options="$options $i"
		fi
	done
	echo "$options"
}
get_not_options_f(){
	local not_options
	for i in "$@"; do
		if ! [ "${i:0:1}" = '-' ]; then
			not_options="$not_options $i"
		fi
	done
	echo "$not_options"
}

#arguments: [ps options] string to search
psg_common_f () { \ps --no-header $(get_options_f "$@") -aux |grep -i $(get_not_options_f "$@") | grep -v grep ; }
psog_common_f () { \ps --no-header $(get_options_f "$@") -eo $pso_format |grep -i $(get_not_options_f "$@") | grep -v grep ; }

psg_f () {
	local proc="$(psog_common_f "$@")"
	if [ -n "$proc" ]; then
		echo "USER       PID  PPID %CPU %MEM     TIME  STARTED NLWP S COMMAND";
		echo "$proc";
	fi ;
}

psgp_f () {
	#local proc="$(\awk '$2=='$1' {print $0}' <<< "$(\ps -eo $pso_format)")"
	local proc="$(\ps -o $pso_format --pid=$1)"
	if [ -n "$proc" ]; then
		#echo "USER       PID  PPID %CPU %MEM     TIME  STARTED NLWP COMMAND";
		echo "$proc";
	fi ;
}

psgz_f () {
	local proc="$(\awk '$9 ~ /Z/ || $NF ~ /<defunct>/ {print $0}' <<< "$(\ps -eo $pso_format)")"
	if [ -n "$proc" ]; then
		echo "USER       PID %CPU %MEM    VSZ   RSS TTY      STAT START   TIME COMMAND";
		echo "$proc";
	fi ;
}

psgs_f () {
	local proc="$(psog_common_f $@)"
	if [ -n "$proc" ]; then
		echo "%CPU %MEM" ;
		\awk '{c+=$4} {m+=$5} END{print c "   " m}' <<< "$proc" ;
	fi ;
}

#kill all with loop
psgk_f () {
	local sig
	local tokill
	if [ -n "$2" ]; then
		sig=$2
	else
		sig=9
	fi

	tokill="$(psog_common_f "$@"|grep -v kill)";
	echo -e "ps of processes about to killed:\n$tokill"
	echo "$tokill"|\awk '{print $2}'| while read -r pid; do
		\kill -$sig $pid || echo -e "Could not kill $pid. Exit status of kill=$?\nSaved info of $pid:\n$(echo "$tokill"|\awk '$2=='"$pid"' {print $0}')\nCurrent info of $pid:\n" && psgp_f $pid
	done;
	#read -r		If this option is given, backslash does not act as an escape character.
}

ntw_ps_f(){
	#notification, waiting for process to end
	#ntw_ps_f pid
	if isnum_f "$1"; then
		local time1=5
		if isnum_f "$2"; then
			time1=$2
		fi
		local ps_exists=1;
		local ps_info="$(\awk '$2=='$1' {print $0}' <<< "$(\ps -eo $pso_format)")"
		while [ -n "$(\awk '$2=='$1' {print $0}' <<< "$(\ps -eo $pso_format)")" ]; do
			if [ "$ps_exists" -eq 1 ]; then
				ps_exists=0
			fi
			sleep $time1
		done
		if [ "$ps_exists" -eq 0 ]; then
			while true; do
				notify-send -t 8000 "Finished $1" "$ps_info";
				echo "Finished $ps_info"
				sleep 15;
			done
		else
			echo "$1 was never running"
		fi ;
	else
		echo "PID expected"
	fi
}

wpsg_f () {
	if [ "$#" -gt 1 ]; then
		if [[ $1 = *$NUMERIC_SEPARATOR* ]]; then
			\watch -n $1 "\ps -eLf|grep -i "$2" | grep -v grep";
		elif [[ $2 = *$NUMERIC_SEPARATOR* ]]; then
			\watch -n $2 "\ps -eLf|grep -i "$1" | grep -v grep";
		fi
	else
		watchn "\ps -eLf|grep -i "$1" | grep -v grep ";
	fi ;
}

watchcontext_f () {
	if [[ $1 = *$NUMERIC_SEPARATOR* ]]; then
		\watch -n $1 "find /proc/$2 -type f -name 'status' 2>/dev/null | xargs grep --color=none ctxt"
	elif [[ $2 = *$NUMERIC_SEPARATOR* ]]; then
		\watch -n $2 "find /proc/$1 -type f -name 'status' 2>/dev/null | xargs grep --color=none ctxt"
	else
		watchn "find /proc/$1 -type f -name 'status' 2>/dev/null | xargs grep --color=none ctxt"
	fi ;
}


monitor_maps_pname_f () { local lpgrep=$(pgrep $1); if isnum "$lpgrep"; then find_monitor /proc/$lpgrep maps ${@:2} ; fi; }
monitor_maps_pnumber_f () { if [ -n "$1" ]; then find_monitor /proc/$1 maps ${@:2} ; fi; }
bak_current_maps_f () { local f; if [ -n "$1" ]; then for f in $(find /proc/$1 -type f -name 'maps' 2>/dev/null); do bak $f ; done; fi; }



dus_f () {
	local temp="$(\du -ha "$@"|tee /dev/tty)"
	echo ""
	sort -hr <<< "$temp"|ccat;
}
du_type_recursive_f () {
	if [ -n "$1" ]; then
		if [[ * = "${1:0:1}" && . = "${1:1:1}" ]]; then
			\find -type f -name "$1" -print0 |\du --files0-from - -hcs
		else
			\find -type f -name "*.$1" -print0 |\du --files0-from - -hcs
		fi
	else
		echo "type needed";
	fi ;
}
du_type_recursive_total_f () { du_type_recursive_f "$1" | tail -1; }


fn_f () { \find "${@:2}" -iname "*$1*" ; }
ffn_f () { \find "${@:2}" -type f -iname "*$1*" ; }
fdn_f () { \find "${@:2}" -type d -iname "*$1*" ; }
fn_date_f () { find "${@:2}" -iname "*$1*" -printf "%C@ %TY/%Tm/%Td %TH:%TM:%TS %p\n" ; }
ffn_date_f () { find "${@:2}" -type f -iname "*$1*" -printf "%C@ %TY/%Tm/%Td %TH:%TM:%TS %p\n" ; }
fdn_date_f () { find "${@:2}" -type d -iname "*$1*" -printf "%C@ %TY/%Tm/%Td %TH:%TM:%TS %p\n" ; }
fn_date_mod_f () {
	#$1 name to search
	#$2 min time
	#$3 minutes to add to min time, to be max time
	#else, find options
	if [ -n "$2" ]; then
		local date_plus
		local time
		if isnum $3; then
			time=$3
		else
			time=1
		fi
		date_plus=$(date -d @$(( $(date -d "$2" "+%s") + (60 * $time) )) "+%Y/%m/%d %H:%M:%S")
		find "${@:4}" -iname "*$1*" -newermt "$2" -not -newermt "$date_plus"
	fi
}
find_videos_sort_size_length_f(){
	#$1 [path]
	#$2 [min size in bytes]
	local extension
	local file
	local size
	local length
	local directory
	local processed
	local min_size
	if [ -n "$1" ]; then
		directory="$1"
	else
		directory='.'
	fi
	if [ -n "$2" ]; then
		min_size="$2"
	else
		min_size='415334400' 			#400MB
	fi
	while read file; do
		extension="$(after_last_occurrence_f "$file" '.')"
		if grep -q -w "$extension" <<< "${video_extensions[@]}" ; then
			size=$(du -b "$file"| cut -f 1)
			if [ "$size" -le "$min_size" ]; then 		#only process files over that size in bytes
				continue
			fi
			length=$(ffprobe -i "$file" -show_entries format=duration -v quiet -of csv="p=0")
			if [[ -n "$length" && "$length" -gt 0 ]]; then
				processed="${processed}$(cut -d '.' -f 1 <<<$((size / length))) $file\n"
			fi
		fi
	done <<< "$(find "$directory" -type f)"
	echo -e "$processed"|sort -rn
}
find_videos_sort_length_f(){
	#$1 [path]
	local extension
	local file
	local length
	local directory
	local processed
	if [ -n "$1" ]; then
		directory="$1"
	else
		directory='.'
	fi
	while read file; do
		extension="$(after_last_occurrence_f "$file" '.')"
		if grep -q -w "$extension" <<< "${video_extensions[@]}" ; then
			length=$(ffprobe -i "$file" -show_entries format=duration -v quiet -of csv="p=0")
			if [[ -n "$length" && "$length" -gt 0 ]]; then
				processed="${processed}$length $file\n"
			fi
		fi
	done <<< "$(find "$directory" -type f)"
	echo -e "$processed"|sort -rn
}

video_duration_seconds_f(){ echo $(ffprobe -i "$1" -show_entries format=duration -v quiet -of csv="p=0") ; }

lsg_f () { lsa "*$1*" ; }
lsa_f () { lsa "$@"|ccat ; }
ll_f () {											#if link: tell me and show me inside, with ccat and options
	local i
	local output
	local temp
	local options
	local files=()
	for i in "$@"; do
		if [ "${i:0:1}" = '-' ]; then
			options="$options $i"
		else
			files+=( "$i" )
		fi
	done
	for i in "${files[@]}"; do
		if [[ -L "$i" && -d "$i" ]]; then
			temp="$(\ls $options "$i" 2>&1; \ls $options "$i"/ 2>&1)"
		else
			temp="$(\ls $options "$i" 2>&1)"
		fi
		if [ -n "$output" ]; then
			output="$output\n$temp"
		else
			output="$temp"
		fi
	done
	if [ -z "$output" ]; then
		output="$(\ls $options "$PWD")"
	fi
	echo -e "$output" | ccat_f
}
lf_f () {											#if link: tell me and show me inside
	local i;
	local existe;
	for i in "$@"; do if [ -e "$i" ]; then existe=true; break; fi ; done
	if [ "$existe" = true ]; then
		for i in "$@"; do if [[ -L "$i" && -d "$i" ]]; then lsa "$i"; lsa_f "$@/"; elif [ -e "$i" ]; then lsa_f "$@"; fi ; done
	else
		if [[ -L "$PWD" && -d "$PWD" ]]; then lsa "$PWD" "$@"; lsa_f "$PWD/" "$@"; else lsa_f "$PWD" "$@"; fi
	fi
}

rllh_f () { if [ "$1" = "" ]; then replace head -n -1 "$HISTFILE"; else replace head -n -$1 "$HISTFILE"; fi ; }

zip_f () {
	local p
	if [ $# -eq 1 ]; then
		if [ "$1" = . ]; then
			p="$(current_folder_trim_f)"
			\zip -r6 "$p".zip *
		else
			p="$(remove_same_characters_end "$1" /)"
			\zip -r6 "$p".zip "$p"
		fi
	elif [ $# -gt 1 ]; then
		local last=${@: -1}		#last argument
		if [[ -f "$last" || -d "$last" ]]; then
			p="$(current_folder_trim_f)"
			\zip -r6 "$p".zip "$@"
		else
			while [ "${last: -4}" = ".zip" ];		#don't care if you write .zip or not
			do
				last=${last:0:-4}
			done
			p="$last"
			\zip -r6 "$p".zip ${@:0:$#}
		fi
	else
		p="$(current_folder_trim_f)"
		\zip -r6 "$p".zip *
	fi
	\zip -T "$p".zip || echo -e "\e[38;05;1m ERROR, ZIP FAILED: zip -T $p.zip returned $?";
}

zip_tex_f () {
	local p
	local fn="$(find * -name '*.tex' -type f)"
	if ! [ $(wc -l <<< "$fn") -gt 1 ]; then
		p="$(before_last_occurrence "$fn" .)"
		\zip -r6 "$p".zip $(find -type f ! -name '*.tex')
		\zip -T "$p".zip || echo -e "\e[38;05;1m ERROR, ZIP FAILED: zip -T $p.zip returned $?";
	else
		echo "too much .tex files:"
		echo "$fn"
	fi
}

gpg_c_f () { gpg -o "$1".e   -c "$1"     "${@:2}" ; }
gpg_d_f () { gpg -o "$1"     -d "$1".e   "${@:2}" ; }

ev_f () { local i; for i in "$@"; do eval echo $"$i"; done ; }

ed_f () { export "$1"="$(eval "$1"_f)" ; }
dc_f () {
	local tmp_var_name
	eval "ed_f $1"
	eval tmp_var_name=\$$1
	echo -n "$tmp_var_name"|pbcopy
	echo "$tmp_var_name saved in \$$1 and clipboard" ;
}
#date: last format option
dD_f () { echo $(\date "+%Y_%m_%d") ; }
dM_f () { echo $(\date "+%Y_%m_%d-%H.%M") ; }
dS_f () { echo $(\date "+%Y_%m_%d-%H.%M.%S") ; }
dN_f () { echo $(\date "+%Y_%m_%d-%H.%M.%S.%N") ; }
date_difference_f () {
	#http://stackoverflow.com/a/27442175
	#egs:			date_difference 3:29:35 2:10:58 s					date_difference 3:29:35.8800056 2:10:58.5400022 %s.%N
	if [[ -n "$1" && -n "$2" ]]; then
		local format
		local d1
		local d2
		if [ -n "$3" ]; then
			format="%$(remove_same_characters_start $3 %)"
		else
			format="%s"
		fi
		d1=$(\date -ud "$1" +"$format")
		d2=$(\date -ud "$2" +"$format")

		if [[ $d2 -gt $d1 ]]; then
			local swap=$d1
			d1=$d2
			d2=$swap
		fi
		\date -ud "0 $d1 sec - $d2 sec" +"%H:%M:$(\tr '[:lower:]' '[:upper:]' <<< "$format")"
	else
		echo "date_difference_f arg1 and arg2 needed"
	fi ;
}
date_difference_var_f () { date_difference_var=$(date_difference_f "$@") && echo "\$date_difference_var=$date_difference_var"; }

#mv and softlink to original path
mv_ln_f () {
	if [ $# -gt 1 ]; then
		local i
		local dest="${@: -1}"
		if [ "$dest" = "." ]; then
			dest="$(readlink -m "$0")"
			dest="$(dirname "$dest")"
		fi
		local name
		for i in "${@:1:$# -1}"
		do
			name="$(after_last_occurrence "$i" /)"
			mv -vi "$i" "$dest/$name" && ln -vsi "$dest/$name" "$i"
		done
	else
		echo "mv_ln_f not enough arguments"
	fi;
}
mv_ln_dot_f () { mv_ln_f "$@" "$dotfiles_path" ; }
mv_ln_data_f () { mv_ln_f "$@" "$data_path" ; }
mv_ln_data_config_f () { mv_ln_f "$@" "$data_path"/.config ; }


mv_all_extensions_f () {
	if [ $# -gt 2 ]; then
		local i
		local dest="${@: -1}"
		for i in "${@:2:$# -1}"
		do
			mv -f "$1.$i" "$dest"
		done
	else
		echo "mv_all_extensions_f not enough arguments"
	fi;
}

mv_latex_tmp_work_f () {
	local fn=$(prev_last_occurrence "$1" .)
	if [ "$2" = "from" ]; then
		(\cd /tmp && mv_all_extensions_f "$fn" $latex_related_extensions "$OLDPWD")
	elif [ -d "$2" ]; then
		(\cd "$2" && mv_all_extensions_f "$fn" $latex_related_extensions "$OLDPWD")
	else
		mv_all_extensions_f "$fn" $latex_related_extensions /tmp
	fi
}
mv_latex_tmp_f () {
	if [ -f "$1" ]; then
		if [ $(after_last_occurrence "$1" .) = "tex" ]; then
			mv_latex_tmp_work_f "$@"
		else
			echo "$1 is not a .tex file"
		fi
	else
		local fn="$(find * -name '*.tex' -type f)"
		if ! [ $(wc -l <<< "$fn") -gt 1 ]; then
			mv_latex_tmp_work_f "$fn"
		else
			echo "too much .tex files:"
			echo "$fn"
		fi
	fi ;
}

return_alias_f () {
	local cv=$(command -v "$1");
	if [ "${cv:0:5}" = "alias" ]; then
		cv=$(prev_last_occurrence $cv "'")
		cv=$(after_last_occurrence $cv "'")
		#"'"	works same as \'
		echo $cv
	else
		echo -1
	fi ;
}
copy_alias_f () { return_alias_f $1|ccopy ; }

copy_regex_case_f () { regex_case_f "$@"|ccopy ; }

munp_f(){
	local file
	local folder
	if [ -n "$2" ]; then
		if [ -f "$1" ]; then
			file="$1"
			folder="$2"
		elif [ -f "$2" ]; then
			file="$2"
			folder="$1"
		else
			echo "A file to uncompress (and optionally a destination folder) are needed"
			return 1
		fi
	else
		if [ -f "$1" ]; then
			file="$1"
			folder="$1"_unp_temp
		else
			echo "A file to uncompress (and optionally a destination folder) are needed"
			return 1
		fi
	fi
	mkdir -p "$folder"
	cp "$file" "$folder"
	cd  "$folder"
	unp "$file" && rm -rf "$file"
	l ;
}

#elog_f () { e $(current_folder_trim_f)_$(dS_f).log ; }
elog_f () { e $(current_folder_trim_f)_$(dS_f).txt; }
etex_f () { e $(current_folder_trim_f).tex ; }

diff_grep_f () { diff -W250 <(\grep -rF "$1" *) <(\grep -rF "$2" *) ; }


less_f () {
	local i
	local options="-R"
	local files=()
	for i in "$@"; do
		if [ "${i:0:1}" = '-' ]; then
			options="$options $i"
		elif [ -f "$i" ]; then
			files+=( "$i" )
		else
			echo "$i cannot be sorted as option or file"
			return 1
		fi
	done
	if [ -z "$files" ]; then
		files=( "-" )
	fi
	for i in "${files[@]}"; do
		if [[ "$i" != "-" && $(du -b "$i"|awk '{print $1}') -gt 500000000 ]]; then #500MB aprox
			\less "$options" "$i"
		else
			\less -M "$options" "$i"
		fi
	done
}
infol_f () { \info "$@" |less ; }

ccat_f () {
	local climit=$((COLUMNS*(LINES-10)))
	local temp
	local line_column
	local line_columns_true

	if [ -d "$1" ]; then
		echo "$1 is a directory"
	elif [ -f "$1" ]; then
		if [ $(du -b "$1"| awk '{print $1}') -lt 4500 ]; then						#if small
			temp="$(\cat "$@")"
			if [[ ${#temp} -gt $climit || $(wc -l < "$1") -gt $LINES ]]; then		#if not small enough
				\less -MR "$1"
			else
				while read line_column; do				#since there are few lines I can do this fast
					if [ "${#line_column}" -gt $COLUMNS ]; then
						line_columns_true=true
						break;							#only needs to be true in one
					fi
				done <<< "$temp"
				if [ -n "$line_columns_true" ]; then
					\less -MR "$1"
				else
					echo "$temp"
				fi
			fi
		else
			\less -R "$1"
		fi
	else
		temp="$(\cat '-')"

		if [[ ${#temp} -gt $climit || $(wc -l <<< "$temp") -gt $LINES ]]; then
			\less -R <<< "$temp"
		else
			while read line_column; do				#since there are few lines I can do this fast
				if [ "${#line_column}" -gt $COLUMNS ]; then
					line_columns_true=true
					break;							#only needs to be true in one
				fi
			done <<< "$temp"
			if [ -n "$line_columns_true" ]; then
				\less -MR <<< "$temp"
			else
				echo "$temp"
			fi
		fi
	fi ;
}


awk_split_alpha_f(){
	awk '{
		split($0, chars, "")
		buffer=""
		for (i=1; i <= length($0); i++) {
			buffer=buffer""chars[i]
			if(chars[i]~/[[:alpha:]]/ || i == length($0)){
				if(length(buffer)>1)
					print buffer
				buffer=""
			}
		}
	}' <<< $1
}

format_time_for_ffmpeg_f(){
	#number before time type, like:
		#1h2m3s
		#3m22s1h
	split="$(awk_split_alpha_f $1)" 			#each param in a row

	h='00'
	m='00'
	s='00'

	if [[ "$split" =~ [hH] ]]; then
		h=$(awk '$0~/[hH]/ {gsub(/[hH]/,"",$0); $0=substr($0,length($0)-1,2); if(length($0)==1){$0="0"$0}; print $0}' <<< "$split")
	fi
	if [[ "$split" =~ [mM] ]]; then
		m=$(awk '$0~/[mM]/ {gsub(/[mM]/,"",$0); $0=substr($0,length($0)-1,2); if(length($0)==1){$0="0"$0}; print $0}' <<< "$split")
	fi
	if [[ "$split" =~ [sS] ]]; then
		s=$(awk '$0~/[sS]/ {gsub(/[sS]/,"",$0); $0=substr($0,length($0)-1,2); if(length($0)==1){$0="0"$0}; print $0}' <<< "$split")
	fi

	echo "$h:$m:$s"
}

ffmpeg_split_f(){
	#strings for time like 1h or 11m3s2h are also accepted instead of regex ..:..:..
	#$1: string word indicating if the time send in $4 is duration/length or end/time (so it needs to be calculated). By default is duration
	#$2: input file
	#$3: start, 00:00:00 if it does not match regex '..:..:..'
	#$4: duration/length or end/time, not used if it does not match regex '..:..:..'
	#$5: [target file]
	#egs:
		#from 0 to 15:30				ffmpeg_split_f whatever file.mp4 not-a-valid-time 00:15:30
		#from 0 to 15:30				ffmpeg_split_f whatever file.mp4 0 00:15:30
		#from 20:00 to 35:30			ffmpeg_split_f ending file.mp4 00:20:00 00:35:30
		#from 20:00 to 35:30			ffmpeg_split_f duration file.mp4 00:20:00 00:15:30
		#from 20:00 to end of media		ffmpeg_split_f whatever file.mp4 00:20:00 [whatever]
	if [ -s "$2" ]; then
		local start="$(format_time_for_ffmpeg_f $3)"
		local end="$(format_time_for_ffmpeg_f $4)"
		local duration
		local dest
		local format="${2##*.}"
		local name="${2%.*}"
		if [[ "$#" -eq 5 && ! -e "$5" ]]; then
			dest="$5"
		else
			local x
			for ((x = 0 ; x <= 100 ; x++)); do
				dest="$name"_split_$x."$format"
				if ! [ -e "$dest" ]; then
					break
				fi
			done
		fi

		if [[ "$end" = '00:00:00' ]]; then
			end="$4"
		fi
 		if [ "$end" =~ '..:..:..' ]; then
			if [[ "$1" =~ 'end' || "$1" = 'time' ]]; then
				#$end is the ending time
				duration="$(date_difference_f $end $start)"
			else
				#$end is the duration time
				duration="$end"
			fi
			ffmpeg -i "$2" -ss "$start" -t "$duration" -codec copy "$dest"
			echo "Split $2 from $start by a duration of $duration, to the file $dest"
		else
			ffmpeg -i "$2" -ss "$start" -codec copy "$dest"
			echo "Split $2 from $start until the end, to the file $dest"
		fi
	fi;
}
ffmpeg_split_mixed_f(){
	#fast written wrap for relive videos with micro in another m4a track
	local vname_split="${2%.*}"_split_0.mp4
	local vname_mixed="${2%.*}"_mixed.mp4
	local aname="${2%.*}".m4a
	local aname_split="${2%.*}"_split_0.m4a
	local aname_extracted="${2%.*}"_extracted.m4a
	local aname_mixed="${2%.*}"_mixed.m4a
	if [[ -s "$2" && -s "$aname" ]]; then
		ffmpeg_split_f "$@" 												#splits video
		ffmpeg_split_f "$1" "$aname" "${@:3:$#}"							#splits audio
		ffmpeg -i "$vname_split" -vn -acodec copy "$aname_extracted" 		#extract audio from splitted video
		ffmpeg -i "$aname_extracted" -i "$aname_split" -filter_complex amerge -ac 2 -c:a aac -q:a 4 "$aname_mixed" 		#mix both audios
		ffmpeg -i "$aname_mixed" -i "$vname_split" -c:v copy -c:a aac "$vname_mixed" 		#splitted video + mixed audio
		rm "$aname_split" "$aname_extracted" "$aname_mixed"
	else
		echo "$aname or $2 does not exist"
	fi
}
ffmpeg_crf_f(){
	#$1 file
	#$2 crf new value
	#$3 if is "replace": replace the original with the new
	#$4 options
	#eg: ffmpeg_crf_f video.mp4 26 replace "-max_muxing_queue_size 1024"
	if [[ ! -s "$1" ]]; then
		echo "\$1=$1 is not a valid file"
		return 1
	fi
	local format="${1##*.}"
	local name="${1%.*}"
	local dest="${name}_crf$2.$format"
	local crf="$2"
	if [[ -z "$crf" || "$crf" -lt 0 || "$crf" -gt 51 ]]; then
		echo -e "$crf is not a valid CRF value!!: Using 26 instead\n\n"
		crf=26
	fi
	ffmpeg -i "$1" $4 -crf "$crf" "$dest"
	if [[ "$?" -eq 0 && "$3" = "replace" ]]; then
		mv -fv "$dest" "$1"
	fi
}
ffmpeg_265_crf_f(){
	#$1 file
	#$2 crf new value
	#$3 if is "replace": replace the original with the new
	#$4 options
	#eg: ffmpeg_265_crf_f video.mp4 26 replace "-max_muxing_queue_size 2048"
	if [[ ! -s "$1" ]]; then
		echo "\$1=$1 is not a valid file"
		return 1
	fi
	local format="${1##*.}"
	local name="${1%.*}"
	local dest="${name}_crf$2.$format"
	local crf="$2"
	if [[ -z "$crf" || "$crf" -lt 0 || "$crf" -gt 51 ]]; then
		echo -e "$crf is not a valid CRF value!!: Using 26 instead\n\n"
		crf=26
	fi
	ffmpeg -i "$1" $4 -vcodec libx265 -x265-params crf="$crf" "$dest"
	if [[ "$?" -eq 0 && "$3" = "replace" ]]; then
		mv -fv "$dest" "$1"
	fi
}
ffmpeg_26X_crf_f(){
	#calls either the 264 or 265 functions based on the current codec of the file
	#ffmpeg_26X_crf_f video.mp4 26 replace "-max_muxing_queue_size 2048"
	local codec="$(ffmpeg -i "$1" -hide_banner 2>&1 |awk '$1=="Stream" && $0~/Video:/ {print $0}'  |awk -F ',' '{print $1}')"
	if [[ "$codec" =~ '264' ]]; then
		ffmpeg_crf_f "$@"
	else
		ffmpeg_265_crf_f "$@"
	fi
}

gif_from_video_f(){
	#$1 video filename
	#[$2] gif filename
	#[$3] temp folder
	#[$4] FPS value
	#[$5] delay value
	#[$6] loop value
	local gif="${1%.*}".gif
	if [ -n "$2" ]; then
		gif="$2"
	fi
	local temp="${3:=temp_folder__gif_from_video_f}"
	local fps="${4:=5}"
	local delay="${5:=20}"
	local loop="${6:=0}"
	local remove=1
	if [ -d "$temp" ]; then
		remove=0
	else
		mkdir -p "$temp"
	fi

	ffmpeg -i "$1" -r $fps "$temp/frame-%03d.jpg" &&
	convert -delay $delay -loop $loop "$temp"/*.jpg "$gif"
	if [ "$remove" -eq 1 ]; then
		rm -rf "$temp"
	fi
}

if [ "$check_lock" != true ]; then
	generic_command_exists_type_f () {
		if ! command_exists_type_f $1 ; then
			echo "$1 not found!"
		fi ;
	}
	generic_command_exists_hash_f () {
		if ! command_exists_hash_f $1 ; then
			echo "$1 not found!"
		fi ;
	}

	check_commands_f () {
		local i
		#once in a while commands, which may not be installed by default or checked individually
		if [ -z "$commands_to_hash_check" ]; then
			local -a commands_to_hash_check
			commands_to_hash_check=( htop wmctrl octave figlet pstree xdotool bash mogrify unp zip gzip tar 7z unrar conky autokey-gtk lsof valgrind xsel pip rename colordiff vimdiff trash trash dropbox make gcc netstat )
		fi
		
		if [ -z "$commands_to_just_check" ]; then
			local -a commands_to_just_check
			commands_to_just_check=( $sublime_text_path $zmanager )
		fi

		for i in "${commands_to_just_check[@]}"
		do
			generic_command_exists_type_f $i
		done

		for i in "${commands_to_hash_check[@]}"
		do
			generic_command_exists_hash_f $i
		done
	}
fi


if [ "$GUI_lock" != true ]; then
	file_manager_f () { if [ "$1" = "" ]; then file_manager "."; elif [ -d "$1" ]; then file_manager "$1"; else file_manager "${1%'/'*}"; fi ; }

	ttv_f () { if [ -n "$1" ]; then \livestreamer twitch.tv/$1 best; else echo "arg1=channel is needed"; fi ; }

	#if not .latexmkrc with pdf_previewer
	latexmkpp_f () {
		local fn
		if [ -z "$1" ]; then
			local find_pdf=$(find -maxdepth 1 -type f  -name "*.pdf");
			if [[ -n "$find_pdf" && $(wc -l <<<"$find_pdf") -eq 1 ]]; then
				fn="$find_pdf"
			else
				echo "No hay solo un pdf que leer o mas de uno, en cuyo caso se debe concretar cual"
			fi
		else
			fn=$(prev_last_occurrence "$1" .).pdf
		fi
		latexmkp "$1" && pdf_manager "$fn" ;
	}
	latexmkpvp_f () {
		local fn
		if [ -z "$1" ]; then
			local find_pdf=$(find -maxdepth 1 -type f  -name "*.pdf");
			if [[ -n "$find_pdf" && $(wc -l <<<"$find_pdf") -eq 1 ]]; then
				fn="$find_pdf"
			else
				echo "No hay solo un pdf que leer o mas de uno, en cuyo caso se debe concretar su archivo.tex"
			fi
		else
			fn=$(prev_last_occurrence "$1" .).pdf
		fi
		(sleep_until_exists_f "$fn" 1 && pdf_manager "$fn") & latexmkpv "$1" ;
	}

	pdf_manager_f () { nohup $pdf_manager "$@" ; }		#compdef

	touch_empty_video_f(){
		local file
		local extension
		local dest
		local found
		if [ -n "$1" ]; then
			file="$@"
		else
			file="$(cpaste)"
		fi

		extension="$(after_last_occurrence_f "$file" '.')"
		if grep -q -w "$extension" <<< "${video_extensions[@]}" ; then
			dest="$file"._empty_
		else
			dest="$file".avi._empty_
		fi

		if [ -e "$file" ]; then
			found="$file"
		else
			found="$(find -maxdepth 1 -type f| grep -F --color=never "$file")"			#in case there are - in the name
		fi
		if [[ -z "$found" || $(wc -l <<<"$found") -eq 0 ]]; then
			echo "NOTHING FOUND by filename $file: NOTHING was performed"
		elif [ $(wc -l <<<"$found") -gt 1 ]; then
			echo "SEVERAL FILES FOUND by filename $file: NOTHING was removed"
			touch "$dest" &&
			echo "Created './$dest'"
		else
			rm -v "$found" &&
			touch "$dest" &&
			echo "Created './$dest'"
		fi
	}
fi
