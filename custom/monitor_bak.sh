#!/usr/bin/env bash
if [ -z "$shellc_path" ]; then shellc_path=. ; fi

. "$shellc_path/functions_misc.sh"
. "$shellc_path/functions_string.sh"

deft=.001
i=$(trim_f "$1")
if [ $? -ne 0 ]; then
	echo "trim_f call resulted in error. Exit code=$?"
	echo "exiting with error code 1..."
	exit 1
fi

bak="$i"_$(date "+%Y_%m_%d-%H.%M.%S.%N")							#bak for diff later
log="$i"_log														#total log

if [ -n "$2" ]; then
	if isnum_nonzero_f $2; then
		t=$2
	else
		echo "default sleeping time in use"
		t=$deft
	fi
else
	t=$deft
fi


bak_file_f () {
	changes=$(diff -W250 --suppress-common-lines $bak "$1")		#check the changes

	if [ -n "$changes" ]; then									#sum can change without file getting really changed (eg conky)
		if [ -f "$1" ]; then		#check again because it could have been deleted just after the while check and in that case we get empty logs
			DATE=$(date "+%Y_%m_%d-%H.%M.%S.%N")				#there is no point in having 2 almost inmmediate informative date calls
			bak="$i"_bak_$DATE									#next loop comparison
			cp "$1" $bak										#bak it
			echo "$changes">>$log								#write the changes. Slow in loop but I want it in "real time"
			echo -e "#diff $bak $1\tprev: $prev\n">>$log		#write some info related
			prev=$DATE
		else
			echo "FILE $1 NOT FOUND"
			exit 0
		fi
	fi
}
trap "bak_file_f $@; exit 0" HUP INT TERM EXIT


sum=$(sha1sum "$1"|awk '{print $1}')
prev=$(date "+%Y_%m_%d-%H.%M.%S.%N")
cp "$1" $bak
while [ -f "$1" ];
do
	nsum=$(sha1sum "$1"|awk '{print $1}')
	if ! [ $sum = $nsum ]; then
		sum=$nsum
		bak_file_f "$@"
	fi

	sleep $t														#loop time lapse
done
