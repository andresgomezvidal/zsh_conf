if [ -z "$shellc_path" ]; then shellc_path=~/Shell/custom/ ; fi

if [ -z "$sourced_functions_string" ]; then
	. "$shellc_path/functions_string.sh"
fi

if [ -z "$sourced_functions_yd" ]; then
	. "$shellc_path/functions_yd.sh"
fi

if [ -z "$youtubedl_history" ]; then
	youtubedl_history=~/.youtube-dl.history
fi

if [ -z "$youtube_download_program" ]; then
	#youtube_download_program="youtube-dl"  # not working anymore
	youtube_download_program="yt-dlp"
fi

#function format just to use local variables
yda_f () {
	if [ -n "$1" ]; then
		local p=$(after_last_occurrence_f "$1" =)
		local f
		local c
		local dest=~/Downloads/yda-$p-$2
		#leave the id part (avoiding annoying slashes and other unnecessary url parts)
		local LOGD=".zzz_download.log"

		grep "$p" "$youtubedl_history"

		if [ -d "$dest" ]; then
			echo "$dest already exists"
		else
			mkdir -p "$dest"
			cd "$dest"
			$youtube_download_program --retries 30 --ignore-errors --extract-audio --audio-format mp3 --audio-quality 0 --add-metadata --output "%(title)s.%(ext)s" "$1" &>"$LOGD"
			for f in *.part; do
				echo "Error?: $f FOUND!"
			done
			for f in *.part-Frag*; do
				echo "Error?: $f FOUND!"
			done
			echo -e "yda_f $(date +%Y_%m_%d): $1 \t\t $(yd_url_title_f $1)" >> "$youtubedl_history"
			echo "dir: ${PWD}"
			cd "$OLDPWD"
		fi
	fi
}


yda_f "$@"
