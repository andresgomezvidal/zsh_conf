#$1:              command to call
#$2:              string to grep ps for
#$3, $4, etc:     arguments for the command

if [ -n "$3" ]; then
	echo "Waiting to call: $1 ${@:3} ..."
	while [ -n "$(ps -ef| grep -v grep| grep -v $0| grep $2)" ]; do
		sleep 1
	done
	eval "$1 ${@:3}"
else
	echo "
	\$1:              command to call
	\$2:              string to grep ps for
	\$3, \$4, etc:     arguments for the command"
fi
