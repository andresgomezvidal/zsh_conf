if [ "$sourced_special_aliases" = true ]; then
	return 0
fi
sourced_special_aliases=true
special_aliases=${special_aliases:="$load_path/special_aliases.zsh"}
alias es='e $special_aliases'


#-g is for global, can appear anywhere in the line
alias -g gg='|g '
alias -g le='|less'

alias -g C='|ccat'

alias yyank='|yank -l -- xsel -b'
alias -g CC='|ccopy'

alias -g T='|tee /dev/tty'

alias -g ,,=' ../'
#alias -g ..=' ../'
#alias -g ...=' ../../'
#alias -g ....=' ../../../'
#alias -g .....=' ../../../../'
alias -g ..2=' ../../'
alias -g ..3=' ../../../'
alias -g ..4=' ../../../../'
alias -g ..5=' ../../../../../'


alias -g tr:='|tr -s ":" "\n" '
alias -g tru='|tr "[:lower:]" "[:upper:]"'
alias -g trl='|tr "[:upper:]" "[:lower:]"'
alias -g trn='|tr -d "\n"'
alias -g trj='|tr " " "\n"'

alias -g prc='|pr -T2 -W$COLUMNS'

alias -g awk:='|awk -F ":" '\''{print $1}'\'' '
alias -g awkp='|awk '\''{print $1}'\'' '
alias -g awki='|awk '\''$1=="" {print $0}'\'' '
alias -g awks='|awk '\''{s+=$1} END{print s}'\'' '
alias -g awkarr='|awk '\''{arr[$1]++} END{ for(i in arr) { print arr[i] " --> " i }}'\'' '

alias -g Smem='--sort=-rss|less'

alias -g ppp='|'

alias -g dne='2> /dev/null'
alias -g dna='&> /dev/null'
alias -g oen='> /dev/null 2 >&1'
alias -g EN='2> & /dev/null'
alias -g EF='2> &1'
alias -g dz='&> /dev/zero'

alias -g while_read='|while read -r i; do echo "i=$i"; done'
alias -g while_i='|while read -r i; do echo "i=$i"; done'

alias -g sa=' `fasd -ais '

alias -g ntw_err=' ; last="$?"; while true; do if [ "$last" -eq 0 ]; then notify-send -t 4000 "Finished"; else notify-send -t 4000 "Finished" "ERROR CODE $last"; fi; sleep 10; done '

alias -g locate_i=' locate -i PATTERN |grep -iEo ".*PATTERN" |sort -u '


#-s is for setting mime type actions, eg alias -s txt=vi (opens txt files in vi). So s=suffix, open with
#alias -s awk='$EDITOR'
#alias -s bak='$EDITOR'
#alias -s c='$EDITOR'
#alias -s h='$EDITOR'
#alias -s htm='$EDITOR'
#alias -s html='$EDITOR'
#alias -s java='$EDITOR'
#alias -s js='$EDITOR'
#alias -s json='$EDITOR'
#alias -s php='$EDITOR'
#alias -s sh='$EDITOR'
#alias -s txt='$EDITOR'
#alias -s xml='$EDITOR'

#alias -s pdf='$pdf_manager'

#alias -s gif='$image_manager'
#alias -s GIF='$image_manager'
#alias -s jpeg='$image_manager'
#alias -s JPEG='$image_manager'
#alias -s jpg='$image_manager'
#alias -s JPG='$image_manager'
#alias -s png='$image_manager'
#alias -s PNG='$image_manager'

#alias -s odt='libreoffice'
