#!/usr/bin/env zsh
#$1 origin (from where to copy/link)

if [ -z "$shellc_path" ]; then shellc_path=. ; fi
if [ -s "$shellc_path/functions_file.sh" ]; then
	. "$shellc_path/functions_file.sh"
else
	echo "Could not load functions_file.sh"
	exit 1
fi
if [ -s "$shellc_path/functions_min.sh" ]; then
	. "$shellc_path/functions_min.sh"
else
	echo "Could not load functions_min.sh"
	exit 1
fi


if [ "$USER" = "root" ];then
	if [ -d "$1" ]; then
		ln_cp_bak_question_f
		if [ -n "$selected_function_ln_cp_bak" ]; then
			ori="$1"
			dest=/root
			mkdir -p "$dest"/.config/nvim

			if reply_q_f "Include Shell files?"; then
				$selected_function_ln_cp_bak "$ori"/Shell "$dest"/Shell
				$selected_function_ln_cp_bak "$ori"/.antigen "$dest"/.antigen
				$selected_function_ln_cp_bak "$ori"/.zgen "$dest"/.zgen
				$selected_function_ln_cp_bak "$ori"/.zshrc "$dest"/.zshrc
				$selected_function_ln_cp_bak "$ori"/.zshenv "$dest"/.zshenv
				$selected_function_ln_cp_bak "$ori"/.inputrc "$dest"/.inputrc
				$selected_function_ln_cp_bak "$ori"/.common_env "$dest"/.common_env
				$selected_function_ln_cp_bak "$ori"/.shell_options "$dest"/.shell_options
				$selected_function_ln_cp_bak "$ori"/.bashrc "$dest"/.bashrc
				$selected_function_ln_cp_bak "$ori"/.tmux.conf "$dest"/.tmux.conf
			fi

			if reply_q_f "Include vim dotfiles?"; then
				$selected_function_ln_cp_bak "$ori"/.vimrc "$dest"/.vimrc
				$selected_function_ln_cp_bak "$ori"/.vimrc_common "$dest"/.vimrc_common
				$selected_function_ln_cp_bak "$ori"/.vimrc_plugins "$dest"/.vimrc_plugins
				$selected_function_ln_cp_bak "$ori"/.vimrc "$dest"/.nvimrc
				$selected_function_ln_cp_bak "$ori"/.vimrc "$dest"/.config/nvim/init.vim
				$selected_function_ln_cp_bak "$ori"/.vim "$dest"/.vim
				$selected_function_ln_cp_bak "$ori"/.vim "$dest"/.nvim
			fi

			if reply_q_f "Include ~/.config/openbox?"; then
				$selected_function_ln_cp_bak "$ori"/.config/openbox "$dest"/.config/openbox
				$selected_function_ln_cp_bak "$ori"/.config/caja "$dest"/.config/caja
				$selected_function_ln_cp_bak "$ori"/.config/tint2 "$dest"/.config/tint2
				$selected_function_ln_cp_bak "$ori"/.config/redshift.conf "$dest"/.config/redshift.conf
				$selected_function_ln_cp_bak "$ori"/.config/terminator "$dest"/.config/terminator
			fi

			if reply_q_f "Include ~ general dotfiles?"; then
				$selected_function_ln_cp_bak "$ori"/lesskeys "$dest"/lesskeys
				$selected_function_ln_cp_bak "$ori"/Templates "$dest"/Templates
				$selected_function_ln_cp_bak "$ori"/.conkyrc "$dest"/.conkyrc
				$selected_function_ln_cp_bak "$ori"/.latexmkrc "$dest"/.latexmkrc
				$selected_function_ln_cp_bak "$ori"/.licenses "$dest"/.licenses
				$selected_function_ln_cp_bak "$ori"/.qjoypad3 "$dest"/.qjoypad3
				$selected_function_ln_cp_bak "$ori"/.selected_editor "$dest"/.selected_editor
				$selected_function_ln_cp_bak "$ori"/.xinitrc "$dest"/.xinitrc
				$selected_function_ln_cp_bak "$ori"/.Xmodmap "$dest"/.Xmodmap
			fi

			if reply_q_f "Include ~/.fonts?"; then
				$selected_function_ln_cp_bak "$ori"/.fonts "$dest"/.local/share/.fonts
			fi

			if reply_q_f "Include ~/.gnome2?"; then
				if [ -d "$dest"/.gnome2 ]; then
					echo -n ".gnome2 already exists, remove it? [Y/n]"
					read REPLY
					echo    # (optional) move to a new line
					if [[ $REPLY =~ ^[Ll]$ ]]; then
						rm -vRfd "$dest"/.gnome2
						$selected_function_ln_cp_bak "$ori"/.gnome2 "$dest"/.gnome2
					else
						echo "Not to remove option selected"
					fi
				fi
			fi
		fi
	else
		echo "First argument should be user \$HOME from where to link"
	fi
else
	echo "Need root permisions... sudo $0 $@:"
	sudo "$0" "$@"
fi
