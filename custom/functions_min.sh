if [ "$sourced_functions_min" = true ]; then
	return 0
fi
sourced_functions_min=true


command_exists_type_f () {
	type "$1" &> /dev/null ;
}
command_exists_hash_f () {
	hash "$1" 2>/dev/null || command -v "$1" &>/dev/null ;
}
#type: false positive if alias (type -P checks the PATH, which ignores aliases and functions)
#hash: false negative if path given (eg $sublime_text_path) or functions (eg antigen)

fall_back_type_f () {
	local i
	for i in "$@"
	do
		if command_exists_type_f $(cut -d ' ' -f1 <<< $i) ; then
			echo $i
			break;
		fi
	done;
}
fall_back_hash_f () {
	local i
	for i in "$@"
	do
		if command_exists_hash_f $(cut -d ' ' -f1 <<< $i) ; then
			echo $i
			break;
		fi
	done;
}
fall_back_f () {
	if [ -z $(eval \$$1 2>/dev/null) ]; then
		local i
		for i in "${@:2}"; do
			if [ -e "$i" ]; then
				export $1="$i"
				return 0
			fi
		done
		if [ "$start_shell_message" != "true" ]; then
			echo "Couldn't export \$$1"
		fi
		return 1
	fi
}
export_z_f (){
	local tmp_var_name
	eval tmp_var_name=\$$1
    if [ -z "$(env| \grep '^'$1=)" ]; then      #export if not exported
        if [ -n "$tmp_var_name" ]; then         #keep value if already exists
            export "$1"="$tmp_var_name"
        else
            export "$1"="$2"
        fi
	fi
}


dir_check_f () {
	local dest="$1".bak_$(date "+%Y_%m_%d-%H_%M_%S")
	if [ -d "$1" ]; then
		if [ "$2" = "false" ]; then
			mv -vi "$1" "$dest"
		else
			echo -n "Destination $1 already exists. Do you want to move-rename it to $dest ?: [Y/n]"
			read REPLY
			echo ""		# (optional) move to a new line
			if ! [[ $REPLY =~ ^[Nn]$ ]]; then
				mv -vi "$1" "$dest"
			fi
		fi
		return 0
	else
		echo "$1 is not a directory"
		return 1
	fi
}

reply_q_f () {
	if [ -n "$2" ]; then
		echo -n "$1: [y/N]"
		read REPLY
		echo ""		# (optional) move to a new line
		if [[ $REPLY =~ ^[Yy]$ ]]; then
			return 0
		else
			return 1
		fi
	else
		echo -n "$1: [Y/n]"
		read REPLY
		echo ""		# (optional) move to a new line
		if [[ $REPLY =~ ^[Nn]$ ]]; then
			return 1
		fi
	fi
}

download_file_f () {
	#$1=file url
	local filename=$(echo -n "${1##*"/"}")
	if command_exists_hash_f wget ; then
		wget "$1" --output-document "$filename" || curl -L -o "$1" "$filename"
	else
		[[ -z "$not_installed_message_wget" ]] && echo "You don't have wget installed, consider installing it" && not_installed_message_wget=true
		if command_exists_hash_f curl; then
			curl -L -o "$1" "$filename"
		else
			echo "Could not find any program to download $filename"
			echo "Exiting the script..."
			exit 2
		fi
		if ! [ $? -eq 0 ]; then
			echo "There was an error ($?) downloading $filename"
			echo "Exiting the script..."
			exit 1
		fi
	fi
}
alias download_file=' download_file_f'
git_clone_download_f () {
	#$1=git repo url
	#$2=final folder name
	#$3=temp folder name
	if command_exists_hash_f git ; then
		#workaround for a bug that only lets you download a git repo if ends with .git
		local git_url
		if [ "${1##*'.'}" = "git" ]; then
			git_url="$1"
		else
			git_url="$1".git
		fi

		if [ -n "$3" ]; then
			git clone "$git_url" "$3" || return 1
			mv -i "$3" "$2" ||
			{
				rm -Rf "$3"; return 1
			}
		else
			git clone "$git_url" "$2" || return 1
		fi
	else
		[[ -z "$not_installed_message_git" ]] && echo "You don't have git installed, consider installing it" && not_installed_message_git=true
		local temp=temp_unzipped		#temp because there is an extra root folder in the zip
		local fn
		local fnz=master.zip
		local git_url="${1%'.git'*}" 					#string before .git

		if [[ "$1" =~ 'github' ]]; then
			fn="$git_url"/zipball/master
		elif [[ "$1" =~ 'gitlab' ]]; then
			local repo="${git_url##*'/'}" 				#repo name
			fn="$git_url/-/archive/master/$repo-master.zip"
		else
			echo "I don't know how to download this :("
			echo "Exiting the script..."
			exit 1
		fi

		if command_exists_hash_f wget; then
			wget "$fn" --output-document "$fnz" || curl -L -o "$fnz" "$fn"
		elif command_exists_hash_f curl; then
			[[ -z "$not_installed_message_wget" ]] && echo "You don't have wget installed, consider installing it" && not_installed_message_wget=true
			curl -L -o "$fnz" "$fn"
		else
			[[ -z "$not_installed_message_curl" ]] && echo "You don't have curl installed, consider installing it" && not_installed_message_curl=true
			echo "Could not find any program to download $fn"
			echo "Exiting the script..."
			exit 2
		fi
		if ! [ $? -eq 0 ]; then
			echo "There was an error ($?) downloading $fn"
			echo "Exiting the script..."
			exit 1
		fi

		if command_exists_hash_f unzip; then
			unzip "$fnz" -d "$temp" || 7z x "$fnz" -o"$temp"
		elif command_exists_hash_f 7z; then
			[[ -z "$not_installed_message_unzip" ]] && echo "You don't have unzip installed, consider installing it" && not_installed_message_unzip=true
			7z x "$fnz" -o"$temp"
		else
			[[ -z "$not_installed_message_7z" ]] && echo "You don't have 7z installed, consider installing it" && not_installed_message_7z=true
			echo "Could not find any program to unzip $fn"
			echo "Exiting the script..."
			exit 2
		fi
		if ! [ $? -eq 0 ]; then
			echo "There was an error ($?) unziping $fn"
			echo "Exiting the script..."
			exit 1
		fi

		local dest
		if [ -n "$2" ]; then
			dest="$2"
		else
			dest=$1
			while [ "${dest: -1}" = / ]
			do
				dest="${dest:0:-1}"
			done
			dest="${dest##*/}"
		fi
		mkdir -p "$dest"
		mv -v "$temp"/*/* "$dest"
		mv -v "$temp"/*/.* "$dest"
		rm -Rv "$temp"
		rm -v "$fnz"
	fi
}
alias git_clone_download=' git_clone_download_f'

github_clone_donwload_f () { git_clone_download_f "https://github.com/$1" "$2" "$3" ; }			#only needs user/repo
alias github_clone_download=' github_clone_download_f'
gitlab_clone_donwload_f () { git_clone_download_f "https://gitlab.com/$1" "$2" "$3" ; }			#only needs user/repo
alias gitlab_clone_download=' gitlab_clone_download_f'

#nohup without file output
#don't know how to have both arguments and filename spaces, so a function for each :(
nohup_f () {
	\nohup "$@" &>/dev/null &
}
alias nohup=' nohup_f'
nohups_f () {			#handling arguments with a space, like "terminator -m" (and line breaks...)
	\nohup $(tr -d '\n' <<< "$@") &>/dev/null &
}
alias nohups=' nohups_f'

ps_exists_f () {
    #true if any process contains the program name
	if [ -n "$(\ps -ef|grep -i $1 |grep -v grep)" ]; then
		return 0;
	else
		return 1;
	fi;
}
alias ps_exists=' nocorrect ps_exists_f'

ps_bpath_exists_f () {
    #true if any process is exactly the program name or any of the executable paths
    local bpath=""
    local ps="$(\ps -eo 'cmd')"
    if [[ -n "$(awk '$0=="'"$1"'"' <<< "$ps")" ]]; then
        return 0
    fi
    type -a "$1" |while read -r i; do
        bpath="$(cut -d ' ' -f3- <<< "$i")"
        if [[ "${bpath:0:1}" = '/' && -n "$(awk '$0=="'"$bpath"'"' <<< "$ps")" ]]; then
            return 0
        fi
    done
    return 1
}
alias ps_bpath_exists=' nocorrect ps_bpath_exists_f'

ifnps_f () { if ! ps_bpath_exists_f $1; then nohup "$@"; fi ; }
alias ifnps=' ifnps_f'
ifnps_second_f () { if ! ps_exists_f $1; then nohup "${@:2}"; fi ; }
alias ifnps_second=' ifnps_second_f'

mv_if_exists_f () {
	if [ -s "$1" ]; then
		echo -n "$1 already exists. Do you want to save (mv/rename) just in case?: [Y/n]"
		read REPLY
		echo ""		# (optional) move to a new line
		if ! [[ $REPLY =~ ^[Nn]$ ]]; then
			if [ -n "$2" ]; then
				dest="$2"
			else
				dest="$1"_$(date "+%Y_%m_%d-%H_%M_%S")
			fi
			mv -vi "$1" "$dest"
		fi
	fi
}
alias mv_if_exists='mv_if_exists_f'

killall_r_f () { killall $@ || local tokill=$(cut -d ' ' -f1 <<< $@) && echo "Trying killall $tokill" && killall $tokill; }
alias killall_r='killall_r_f'

install_try_f(){
	if command_exists_hash_f apt-get ; then
		sudo apt-get install "$1" -y
	elif command_exists_hash_f pacman ; then
		sudo pacman -S --needed "$1" --noconfirm
	elif command_exists_hash_f dnf;  then
		sudo dnf install "$1" -y
	elif command_exists_hash_f yum;  then
		sudo yum install "$1" -y
	else
		echo "NOT FOUND ANY PACKAGE MANAGER (apt-get, dnf, yum or pacman)"
	fi
}
alias install_try='install_try_f'

echo_f(){
	local Echo_color_code='\033[0;33m'
	local Echo_color_code_null='\033[0m' # No Color
	local options
	local Echo_text
	for i in "$@"; do
		if [ "${i:0:1}" = '-' ]; then
			options="$options $i"
		else
			Echo_text+=( "$i" )
		fi
	done
    echo -e $options "${Echo_color_code}$Echo_text${Echo_color_code_null}"
}

error_f(){
    echo_f "Error $? ejecutando el comando anterior"
    exit 1
}
