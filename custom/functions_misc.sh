if [ "$sourced_functions_misc" = true ]; then
	return 0
fi
sourced_functions_misc=true

isnum_f () {		 #http://stackoverflow.com/a/22106305
	if [ $(awk -v a="$1" 'BEGIN {print (a == a + 0)}') -eq 1 ]; then
		return 0
	else
		return 1
	fi;
}
alias isnum=' isnum_f'

isnum_nonzero_f () { if [[ $(isnum_f "$1") -eq 0 && "$1" -ne 0 ]]; then return 0; else return 1; fi; }
alias isnum_nonzero=' isnum_nonzero_f'

isinteger_f () {		#http://stackoverflow.com/a/2210386
	if [[ $1 =~ ^-?[0-9]+$ ]]; then
		return 0
	else
		return 1
	fi
}
alias isinteger=' isinteger_f'

isinteger_nonzero_f () { if [[ $(isinteger_f "$1") -eq 0 && ! "$1" -eq 0 ]]; then return 0; else return 1; fi; }
alias isinteger_nonzero=' isinteger_nonzero_f'
