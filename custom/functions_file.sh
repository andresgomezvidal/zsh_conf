if [ "$sourced_functions_file" = true ]; then
	return 0
fi
sourced_functions_file=true


if [ -z "$shellc_path" ]; then shellc_path=. ; fi

if [ -z "$sourced_functions_string" ]; then
	. "$shellc_path/functions_string.sh"
fi


remove_duplicate_lines_f () {
	local f=/tmp/rm_dulicates_$(date "+%Y_%m_%d-%H_%M_%S_%N")
	awk '!NF || !seen[$0]++' "$1">$f
	mv -f $f "$1"
}
alias remove_duplicate_lines='remove_duplicate_lines_f'
alias rm_duplicate_lines='remove_duplicate_lines'

symlink_or_copy_f () {
	local REPLY
	if [ -L "$1" ]; then
		echo -n "$1 is a symlink. Do you want $2 to be a textual copy or a symlink?: [C/l]"	#http://superuser.com/a/556016
		read REPLY
		if [[ $REPLY =~ ^[Ll]$ ]]; then
			echo "Option 'SoftLink' selected"
			rsync -az "$@"
		else
			echo "Option 'Copy' selected"
			rsync -azL "$@"
		fi
		echo ""		# (optional) move to a new line
	else
		rsync -az "$@"
	fi
}

rsync_f () {
	if [ -d "$1" ]; then
		rsync -az "$1"/ "$2"
	else
		rsync -az "$1" "$2"
	fi
}
alias rsf='rsync_f'

bak_f () {
	local now
	local ori
	local name_dest
	local dest
	local end=$#
	local bak=.bak
	local REPLY

	if [ "$1" = true ]; then
		now=_$(date "+%Y_%m_%d-%H_%M_%S_%N")
	fi

	if [ $# -lt 2 ]; then
		symlink_or_copy_f "$PWD/" "$PWD""$bak""$now"
	elif [ $# -gt 2 ]; then
		last="${@: -1}"
		if ! [ -e "$last" ]; then
			dest="$last/"
			end=-1
			mkdir -p "$dest"
		else
			if [ -d "$last" ]; then
				echo -n "Choose destination: $PWD [1], $last [2] or its current directory for each file [3](default)"
				read REPLY
				echo ""		# (optional) move to a new line
				if [[ $REPLY = 1 ]]; then
					dest="$PWD/"
				elif [[ $REPLY = 2 ]]; then
					dest="$last/"
					end=-1
				fi
			else
				echo -n "Choose destination: $PWD [1] or its current directory for each file [2](default)"
				read REPLY
				echo ""		# (optional) move to a new line
				if [[ $REPLY = 1 ]]; then
					dest="$PWD/"
				fi
			fi
		fi
	fi

	for i in "${@:2:$end}"
	do
		if [ -d "$i" ]; then
			ori="$i/"
		else
			ori="$i"
		fi

		name_dest=$(remove_same_characters_end_f "$i" /)
		name_dest=$(after_last_occurrence_f "$name_dest" /)

		if [ -n "$dest" ]; then
			symlink_or_copy_f "$ori" "$dest""$name_dest""$bak""$now"
		else
			symlink_or_copy_f "$ori" "$i""$bak""$now"
		fi
	done
}
alias bak='bak_f true'
alias baks='bak_f false'

unbak_f () {
	local ori
	local dest

	if [ -d "$1" ]; then
		ori="$1/"
	else
		ori="$1"
	fi
	dest=$(remove_same_characters_end_f "$1" /)

	#make sure dest ends as ori without .bak*
	while [[ "$dest" =~ ".bak" ]]
	do
		dest=$(prev_last_occurrence_f "$dest" ".bak")
	done
	
	symlink_or_copy_f "$ori" "$dest"
}
alias unbak='unbak_f'

mbak_f () {
	local dest
	local now
	local name_dest
	local i
	local last
	local bak=.bak
	local end=$#
	local REPLY

	if [ "$1" = true ]; then
		now=_$(date "+%Y_%m_%d-%H_%M_%S_%N")
	fi

	last="${@: -1}"
	if ! [ -e "$last" ]; then
		dest="$last/"
		end=-1
		mkdir -p "$dest"
	elif [ $# -gt 2 ]; then
		if [ -d "$last" ]; then
			echo -n "Choose destination: $PWD [1], $last [2] or its current directory for each file [3](default)"
			read REPLY
			echo ""		# (optional) move to a new line
			if [[ $REPLY = 1 ]]; then
				dest="$PWD/"
			elif [[ $REPLY = 2 ]]; then
				dest="$last/"
				end=-1
			else
				end=-1
			fi
		else
			echo -n "Choose destination: $PWD [1] or its current directory for each file [2](default)"
			read REPLY
			echo ""		# (optional) move to a new line
			if [[ $REPLY = 1 ]]; then
				dest="$PWD/"
			else
				end=-1
			fi
		fi
	fi

	for i in "${@:2:$end}"
	do
		name_dest=$(remove_same_characters_end_f "$i" /)
		name_dest=$(after_last_occurrence_f "$name_dest" /)
		if [ -n "$dest" ]; then
			mv -i "$i" ${dest}${name_dest}${bak}${now}
		else
			mv -i "$i" ${i}${bak}${now}
		fi
	done
}
alias mbak='mbak_f true'
alias mbaks='mbak_f false'

swap_f () {
	local temp_file="${1}_swap_f_temp_$(\date "+%Y_%m_%d-%H.%M.%S")"
	if [[ -e "$1" && -e "$2" ]]; then
		mv -i "$1" $temp_file && mv "$2" "$1" && mv $temp_file "$2"
	else
		echo "$1 or $2 does not exist"
	fi
}
alias swap='swap_f'
sbak_f () {		#swap between file and file.bak or file.bak* if none
	local found
	if [ -n "$1" ]; then
		if [ -e "$1".bak ]; then
			swap_f "$1" "$1".bak
		else
			found=$(find -name "$1.bak*")
			if [ $(wc -l <<< "$found") -eq 1 ]; then
				swap_f "$1" "$found"
			else
				echo "cannot swap with only one .bak, not only one match found"
			fi
		fi
	else
		local i
		local bf
 		for i in $(find -name '*.bak'); do
			bf="$(before_last_occurrence $i .)"
			if [ -e "$bf" ]; then
				if [ -z "$found" ]; then
					found="$bf"
				else
					echo "cannot swap with only one .bak, too much matches found"
				fi
			fi
		done
		if [ -n "$found" ]; then
			swap_f "$found" "$found".bak
		else
			local bfound
			for i in $(find -name '*.bak*'); do
				bf="$(before_last_occurrence $i .bak)"
				if [ -e "$bf" ]; then
					if [ -z "$found" ]; then
						found="$bf"
						bfound="$i"
					else
						echo "cannot swap with only one .bak, too much matches found"
					fi
				fi
			done
			if [ -n "$found" ]; then
				swap_f "$found" "$bfound"
			else
				echo "cannot swap with only one .bak, no matches found"
			fi
		fi
	fi ;
}
alias sbak='sbak_f'

replace_f () {
	local last=${@: -1}		#last argument
	local argst
	local f
	local i

	if [ -f "$last" ]; then
		argst=
		for i in "$@"; do
			argst="$argst"_$(trim_f "$i")
		done

		f=/tmp/output_replace"$argst"
		#shouldn't have any conflict with that name

		"$@" > $f && mv -f $f "$last"
	else
		echo "Last argument, which is $last, is not a file"
	fi
}
alias replace='replace_f'


fd_rm_f () {
	if [ $# -gt 1 ]; then
		local last=${@: -1}		#last argument
		find "$1" -name "$last" -type d "$@:2" -print0|xargs -0 rm -r --
	else
		find -name "$1" -type d -print0|xargs -0 rm -r --
	fi

	#xargs executes the command passed as parameters, with the arguments passed to stdin.
	#This is using rm -r to delete the directory and all its children.
	#The -- denotes the end of the arguments, to avoid a path starting with - from being treated as an argument.
	#-print0 tells find to print \0 characters instead of newlines; and -0 tells xargs to treat only \0 as argument separator.
	#This is calling rm with many directories at once, avoiding the overhead of calling rm separately for each directory.
	#http://superuser.com/questions/602385/find-and-delete-all-the-directories-named-test-in-linux
}
alias fd_rm='fd_rm_f'
alias fd_delete='fd_rm';


sleep_until_exists_f () {
	local sleep_time
	if isnum_nonzero "$2"; then
		sleep_time=$2
	elif isnum_nonzero "$WATCHN"; then
		sleep_time=$WATCHN
	else
		sleep_time=2
	fi
	while [ ! -f "$1" ]
	do
		\sleep $sleep_time
	done ;
}
alias sleep_until_exists=' sleep_until_exists_f'

exists_bak_f () {
	local REPLY
	if [ -e "$1" ]; then
		echo -n "$1 Already exists. Do you want to rename it?: [Y/n]"
		read REPLY
		echo ""		# (optional) move to a new line
		if ! [[ $REPLY =~ ^[Nn]$ ]]; then
			mbak_f true "$1" "${1%'/'*}"
		fi
	fi ;
}
alias exists_bak='exists_bak_f'

ln_bak_f () {
	if [ -n "$2" ]; then
		exists_bak_f "$2"
		ln -s "$1" "$2"
	else
		echo "Two arguments needed"
	fi
}
alias ln_bak='ln_bak_f'

cp_bak_f () {
	if [ -n "$2" ]; then
		exists_bak_f "$2"
		rm -Rf "$2"
		rsync_f "$1" "$2"
	else
		echo "Two arguments needed"
	fi
}
alias cp_bak='cp_bak_f'

ln_cp_bak_f () {
	local REPLY
	echo -n "Copy, soft link or nothing for $@?: [C/l/n] (C-c to exit)"
	read REPLY
	if [[ $REPLY = [Nn] ]]; then
		echo "Option 'Do nothing'  selected"
	elif [[ $REPLY =~ ^[Ll]$ ]]; then
		echo "Option 'SoftLink' selected"
		ln_bak_f "$@"
	else
		echo "Option 'Copy' selected"
		cp_bak_f "$@"
	fi
	echo ""		# (optional) move to a new line
}

ln_cp_bak_question_f () {
	local REPLY
	echo -n "Do you want to use the same method (copy or slink) for ALL FILES (A) or CHOOSE INDIVIDUALLY (i)?: [A/i] (C-c to exit)"
	read REPLY
	echo ""		# (optional) move to a new line
	if [[ $REPLY =~ ^[Ii]$ ]]; then
		echo "Individually link or copy selected"
		selected_function_ln_cp_bak=ln_cp_bak_f
	else
		echo "Globally link or copy selected"
		echo -n "Copy or soft link files ?: [C/l] (C-c to exit)"
		read REPLY
		if [[ $REPLY =~ ^[Ll]$ ]]; then
			echo "Option 'SoftLink' selected"
			selected_function_ln_cp_bak=ln_bak_f
		else
			echo "Option 'Copy' selected"
			selected_function_ln_cp_bak=cp_bak_f
		fi
		echo ""		# (optional) move to a new line
	fi
}

find_permutations_ul_f () {
	local find_command
	for i in $(permutations_ul_f $@|tr " " "\n"); do
		if [ -z "$find_command" ]; then
			find_command="find -name '*.$i'";
		else find_command="$find_command -o -name '*.$i'";
		fi ;
	done ;
	eval "$find_command"
}
alias find_permutations_ul='find_permutations_ul_f'

fall_back_dir_f () {
	local i
	for i in "$@"; do
		if [ -e "$i" ]; then
			echo -n "$i"
			break
		fi
	done
	echo -n ""
}

mtrim_f(){
	local i
	for i in "$@"; do
		\mv -vi "$i" "$(trim_f "$i")"
	done
}
alias mtrim='mtrim_f'


#copied from /etc/profile
appendpath_f () {
    case ":$PATH:" in
        *:"$1":*)
            ;;
        *)
            export PATH="$PATH:$1"
    esac
}
alias appendpath=' appendpath_f'



grep_sed_echo_f(){
	#si no existe en el archivo lo añade, si existe lo reemplaza con el valor correspondiente
	#$1 Parámetro
	#$2 Valor
	#$3 Archivo
	#$4 [símbolo de separación entre parámetro y valor] por defecto es '='
	local s='='
	if [ -n "$4" ]; then
	    s="$4"
	fi

	if [ -n "$(grep "^[ \t]*$1" "$3")" ]; then                                  #si lo encuentra y solo hay espacios o tabs antes
		sed -i "s/^\(\s*\)$1\(\s*\)$s\(\s*\)\(.*\)/\1$1\2$s\3$2/g" "$3"          #sustituye el valor, y conserva el espaciado (espacios y tabs) de antes, incluyendo antes del match, del = y del valor
	else
		echo "$1=$2" >> "$3"
	fi
}
alias grep_sed_echo='grep_sed_echo_f'

awk_after_f(){
    #$1 match config
    #$2 print config
    #$3 origen
    #$4 [destino], opcional, si no se usa el mismo que origen y se hace una backup

    #por ejemplo
        #awk_after_f '$1~/\[global\]/' 'printf "\t%s\n", "winbind enum users = yes"' '/etc/samba/smb.conf'
        #awk_after_f '$1~/global/' 'print "winbind enum users = yes"' '/etc/samba/smb.conf.bak' '/etc/samba/smb.conf'

    if [ -n "$4" ]; then
    	awk '{print} '"$1"' {'"$2"'}' "$3" > "$4"
    else
    	local bak="$3".bak.$(date +"%Y_%m_%d--%H-%M-%S")
    	mv "$3" "$bak"
    	awk '{print} '"$1"' {'"$2"'}' "$bak" > "$3"
    fi
}
alias awk_after='awk_after_f'
