if [ "$sourced_functions_string" = true ]; then
	return 0
fi
sourced_functions_string=true


remove_same_characters_end_f () {
	#eg:	a=$(remove_same_characters_end_f dir///////// /)
	#		returns dir
	local l1=${#1}
	local l2=${#2}
	local string
	local char

	if [[ $l1 -gt 0 && $l2 -gt 0 ]]; then
		if ! [[ $l1 -gt 1 && $l2 -gt 1 ]]; then
			#order doesn't matter
			if [ $l2 -ge $l1 ]; then
				string="$2"
				char="$1"
			else
				string="$1"
				char="$2"
			fi
			
			while [ "${string: -1}" = "$char" ]
			do
				string="${string:0:-1}"
			done
			
			echo -n "$string"
		else
			echo "both arguments 1=$1 and 1lenght=$l1 and 2=$2 and 2lenght=$l2 are greater than 1, so there is no defined char and string"
		fi
	else
		echo "one of the arguments 1=$1 or 2=$2 is empty"
	fi
}
alias remove_same_characters_end=' remove_same_characters_end_f'

remove_same_characters_start_f () {
	#eg:	a=$(remove_same_characters_start_f /////////foo /)
	#		returns foo
	local l1=${#1}
	local l2=${#2}
	local string
	local char

	if [[ $l1 -gt 0 && $l2 -gt 0 ]]; then
		if ! [[ $l1 -gt 1 && $l2 -gt 1 ]]; then
			#order doesn't matter
			if [ $l2 -ge $l1 ]; then
				string="$2"
				char="$1"
			else
				string="$1"
				char="$2"
			fi
			
			while [ "${string:0:1}" = "$char" ]
			do
				string="${string:1}"
			done
			
			echo -n "$string"
		else
			echo "both arguments 1=$1 and 1lenght=$l1 and 2=$2 and 2lenght=$l2 are greater than 1, so there is no defined char and string"
		fi
	else
		echo "one of the arguments 1=$1 or 2=$2 is empty"
	fi
}
alias remove_same_characters_start=' remove_same_characters_start_f'

after_first_occurrence_f () {
	#order doesn't matter
	#returns everything after first char occurrence
	if [ $(wc -c <<< "$1") -ge $(wc -c <<< "$2") ]; then
		echo -n "${1#*"$2"}"
	else
		echo -n "${2#*"$1"}"
	fi
}
alias after_first_occurrence=' after_first_occurrence_f'

after_last_occurrence_f () {
	#order doesn't matter
	#returns everything after last char occurrence
	if [ $(wc -c <<< "$1") -ge $(wc -c <<< "$2") ]; then
		echo -n "${1##*"$2"}"
	else
		echo -n "${2##*"$1"}"
	fi
}
alias after_last_occurrence=' after_last_occurrence_f'

prev_last_occurrence_f () {
	#order doesn't matter
	#returns everything previous last char occurrence
	if [ ${#1} -ge ${#2} ]; then
		echo -n "${1%"$2"*}"
	else
		echo -n "${2%"$1"*}"
	fi
}
alias prev_last_occurrence=' prev_last_occurrence_f'
alias before_last_occurrence=' prev_last_occurrence_f'

prev_first_occurrence_f () {
	#order doesn't matter
	#returns everything previous first char occurrence
	if [ ${#1} -ge ${#2} ]; then
		echo -n "${1%%"$2"*}"
	else
		echo -n "${2%%"$1"*}"
	fi
}
alias prev_first_occurrence=' prev_first_occurrence_f'
alias before_first_occurrence=' prev_first_occurrence_f'

trim_f () {
	local var="$1"
	var="${var#"${var%%[![:space:]]*}"}"   #remove leading whitespace characters
	var="${var%"${var##*[![:space:]]}"}"   #remove trailing whitespace characters
	var=${var//:/_}                        #replace all : with _, in case ssh
	var=${var//\~/_}                       #replace all ~ with _, in case quoted
	#var=${var// /_}                       #replace all remaining whitespace characters with _

	#echo -n "$var"

	echo -n ${var//[\/ ]/_}
}
alias trim=' trim_f'

current_folder_trim_f () { echo $(trim_f "$(after_last_occurrence_f $PWD /)") ; }
alias current_folder_trim=' current_folder_trim_f'

regex_case_f () {
	local w
	local wr
	local l
	local L
	local TR
	local i
	
	for w in "${@}"
	do
		wr=
		for ((i=0; i<${#w}; i++))
		do
			l=${w:$i:1}
			l=$(tr '[:upper:]' '[:lower:]'<<<$l)
			L=$(tr '[:lower:]' '[:upper:]'<<<$l)
			wr="$wr"["$L""$l"]
		done
		TR="$TR $wr"
	done
	TR="${TR#"${TR%%[![:space:]]*}"}"		#remove leading whitespace characters
	echo -n "$TR"
}
alias regex_case=' regex_case_f'

correct_spaces_name_f () {
	local input
	local var
	[ $# -ge 1 ] && input="$1" || input="-"

	if [ "$input" = "-" ]; then
		local temp=$(cat "$input");
		var=$(sed 's/ /\\ /g'<<<"$temp")
	else
		var=$(sed 's/ /\\ /g'<<<"$input")
	fi

	echo -n "$var"
}
alias correct_spaces_name=' correct_spaces_name_f'

genpass_f () {
	local l=$1
	local i
	local j
	local loops=0
	local same
	local same_total
	local letters
	local pass
	local MAX_LOOPS=1000
	[ "$l" = "" ] && l=20

	while [ "$loops" -lt $MAX_LOOPS ]; do
		loops=$((loops + 1))

		#generates the password, with letters and special characters
		pass=`tr -dc "\!\-\;a-zA-Z0-9@#$%^&*+=_?:.,|/(){}[]><~" < /dev/urandom | head -c ${l} | xargs`

		#discards the password if a character appears more than 2 times or if the same appears twice in a row (too common with this method)
		letters=0
		same_total=0
		for (( i=0; i<${#pass}; i++ )); do
			same=0
			for (( j=0; j<${#pass}; j++ )); do
				if [[ "$i" -ne "$j" && "${pass:$i:1}" == "${pass:$j:1}" ]]; then
					same=$((same + 1))
				fi
			done

			if [[ "$same" -gt 1 || "${pass:$i:1}" == "${pass:$((i+1)):1}" ]]; then
				#cannot use continue inside loop, so this ends this loop and secures the true evaluation to the next continue
				letters=$l
				i=${#pass}
			fi
			if [[ "${pass:$i:1}" =~ [a-zA-Z] ]]; then
				letters=$((letters + 1))
			fi
			same_total=$((same_total + same))
		done

		#discards the password if there are too many letters
		if [ "$letters" -gt $(bc -l <<< "$l / 1.5"|cut -d '.' -f1) ]; then
			continue
		fi

		#discards the password if there are too many repetitions
		if [ "$same_total" -gt $(bc -l <<< "$l / 2.5"|cut -d '.' -f1) ]; then
			continue
		fi

		#if all is correct, then this is reached, ending the loop
		break
	done

	if [ "$loops" -lt $MAX_LOOPS ]; then
		echo "$pass"
	else
		return 1
	fi;
}
genpassln_f () { 													#only letters and numbers
	local l=$1
	[ "$l" = "" ] && l=20
	tr -dc "a-zA-Z0-9" < /dev/urandom | head -c ${l} | xargs
}
alias genpass=' genpass_f'
alias genpassln=' genpassln_f'

after_first_occurrence_line_f () {
	local last=${@: -1}		#last argument
	if [ -s "$last" ]; then
		sed -ne '/'${@:1:-1}'/,$p' $last ;
	else
		sed -ne '/'$@'/,$p' ;
	fi
}
before_first_occurrence_line_f () {
	local last=${@: -1}		#last argument
	if [ -s "$last" ]; then
		sed '/'${@:1:-1}'/q' $last ;
	else
		sed '/'$@'/q' ;
	fi
}

alias after_first_occurrence_line=' after_first_occurrence_line_f'
alias before_first_occurrence_line=' before_first_occurrence_line_f'

permutations_ul_f () {							#upper and lower case permutations
	local e="echo ";
	local l
	local L
	local i
	for ((i=0;i<${#1};i++)); do
		l=${1:$i:1}; l=$(tr '[:upper:]' '[:lower:]'<<<$l);
		L=$(tr '[:lower:]' '[:upper:]'<<<$l);
		e="$e{$l,$L}";
	done;
	e="$(eval "$e")";
	for i in "${@:2}"; do
		e="$(sed 's/'"$i"'//g' <<< "$e")"
		#e="$(akw '$0=='$i' {print $0}' <<< "$e")"
	done
	echo "$e";
}
alias permutations_ul=' permutations_ul_f'

pcut_f () {
	local pos="-f"$2
	cut -d "$1" "$pos" <<< "$3"

	#$1=char indicating from where to cut
	#$pos(starting from 1=left) indicates which part is retrieved (in this case left=1 or right=2), middle is ":"
	#$3 is the text needed to be cut

	#egs
		#	cut -d : -f1 <<< "cortame:por-la-mitad"
		#can now be simplified to
		#	pcut : 1 "cortame:por-la-mitad"
		#resulting in
		#			cortame

		#	pcut : 2 "cortame:por-la-mitad"
		#		por-la-mitad

		#to get the last you can reverse it
		#	echo '1,2,3' | rev | cut -d',' -f 1|rev
}
alias pcut=' pcut_f'

#https://www.reddit.com/r/linuxquestions/comments/5kjwgr/help_parsing_a_log_with_sed_or_awk/
grep_between_if_match_keep_lines_f(){
	if [ -z "$4" ]; then
		echo "\$1: pattern to grep, \$2: pattern marking start, \$3: pattern marking end, \$4: file"
	else
		sed -n -e "/$2/,/$3/{H;/$2/h;}" -e "/$3/{g;/$1/p;}" "$4"
	fi
}
alias grep_between_if_match_keep_lines=' grep_between_if_match_keep_lines_f'
