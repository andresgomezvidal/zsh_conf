if [ "$sourced_functions_c" = true ]; then
	return 0
fi
sourced_functions_c=true


run_f () {
	local var=$(find . -maxdepth 1 -type f ! -name "*.sh" ! -name "*.py*" ! -name "*.bak*" -executable);
	local num=$(wc -l <<<"$var")

	if [ -n $var ]; then
		if [ $num -eq 1 ]; then
			$var ${@}
		else
			if [ -x "$executable_default" ]; then
				echo -e "Chosed $executable_default\nThere were several ($num) files to execute: \n$var";
				./$executable_default "$@"				#my default executable file
			else
				echo -e "Error: several ($num) files to execute: \n$var";
			fi
		fi
	else
		echo "error: none files to execute found";
	fi

	#ejemplo ejecutable: con 	find . la salida es ./ejecutable	si quisiera salida	ejecutable usaria 	find *
}
alias run=' run_f'

rvalg_f () {
	local var=$(find . -maxdepth 1 -type f ! -name "*.sh" ! -name "*.py*" ! -name "*.bak*" -executable);
	local num=$(wc -l <<<"$var")

	if [ -n $var ]; then
		if [ $num -eq 1 ]; then
			valgrind --leak-check=full -v $var "$@"
		else
			if [ -x "$executable_default" ]; then
				echo -e "Chosed $executable_default\nThere were several ($num) files to execute: \n$var";
				valgrind --leak-check=full -v ./$executable_default "$@"
			else
				echo -e "Error: several ($num) files to execute: \n$var";
			fi
		fi
	else
		echo "error: none files to execute found";
	fi

	#ejemplo ejecutable: con 	find . la salida es ./ejecutable	si quisiera salida	ejecutable usaria 	find *
}
alias rvalg=' rvalg_f'

gcc_one_liner_f () {
	if [ -n "$1" ]; then
		local f=$(cut -d "." -f1 <<<"$1");
		gcc -Wall -g "$f".c -o "$f";
	else
		echo "no file to compile";
	fi
}
alias gcc_one_liner=' gcc_one_liner_f'
