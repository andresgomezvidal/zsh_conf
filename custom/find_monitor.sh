#!/usr/bin/env bash
if [ -z "$shellc_path" ]; then shellc_path=. ; fi

. "$shellc_path/functions_misc.sh"

deftl=1
m="arg1 (\$1=$1) should be directory base to search (eg:/proc/22 or .)\narg2 (\$2=$2) should be file name (eg: maps)\narg3 (\$3=$3) !=0 is for sleeping time in monitor_bak loop\narg4 (\$4=$4) !=0 is for sleeping time in checking for entries loop (this loop, default is $deftl)"


if [[ -n "$2" ]]; then
	LIST=(" ")															#empty element so it can start the loop the first time
	PLIST=()															#list for pids, to kill later
	if [ -n "$4" ]; then
		if isnum_nonzero_f $4; then
			tl=$4
		else
			echo -e $m
			echo "default sleeping time in use"
			tl=$deftl
		fi
	else
		tl=$deftl
	fi


	kill_clean_f() {
		for pid in "${PLIST[@]}"
		do
			kill -1 $pid
		done
		exit 0
	}
	trap "kill_clean_f" HUP INT TERM EXIT


	while [ -d "$1" ];													#in case hits are added after start
	do
		for f in $(find "$1" -type f -name "$2" 2>/dev/null); do
			eq=false
			for i in "${LIST[@]}"										#check if it's in the list, if not call the function and add it
			do
				if [ "$i" = "$f" ]; then
					eq=true
					break
				fi
			done
			if [ $eq = false ]; then
				LIST+=("$f")
				$shellc_path/monitor_bak.sh "$f" $3 &
				PLIST+=("$!")
			fi
		done
		sleep $tl
	done
else
	echo -e $m
fi

