if [ "$sourced_ctheme" = true ]; then
	return 0
fi
sourced_ctheme=true
ctheme=${ctheme:="$load_path/ctheme.zsh"}

# the idea of this theme is to contain a lot of info in a small string, by
# compressing some parts and colorcoding, which bring useful visual cues,
# while limiting the amount of colors and such to keep it easy on the eyes.
# When a command exited >0, the timestamp will be in red and the exit code
# will be on the right edge.
# The exit code visual cues will only display once.
# (i.e. they will be reset, even if you hit enter a few times on empty command prompts)

typeset -A host_repr

# translate hostnames into shortened, colorcoded strings
host_repr=('dieter-ws-a7n8x-arch' "%{$fg_bold[green]%}ws" 'dieter-p4sci-arch' "%{$fg_bold[blue]%}p4")

# local time, color coded by last return code
time_enabled="%(?.%{$fg[green]%}.%{$fg[red]%})%T%{$reset_color%}"
time_disabled="%(?..%{$fg[red]%}%? ↵ %{$reset_color%})$time_enabled"

time=$time_enabled

# user part, color coded by privileges
local user="%(!.%{$fg_bold[red]%}.%{$fg[yellow]%})%n%{$reset_color%}"

# Hostname part.  compressed and colorcoded per host_repr array
# if not found, regular hostname in default color
local host="@${host_repr[$HOST]:-$HOST}%{$reset_color%}"

# Compacted $PWD
local pwd="%{$fg_bold[green]%}%c%{$reset_color%}"

if [ "$keybinding_mode" =~ "vi" ]; then
	#http://pawelgoscicki.com/archives/2012/09/vi-mode-indicator-in-zsh-prompt/
	vim_ins_mode="%{$fg_bold[blue]%}[I]%{$reset_color%}"
	vim_cmd_mode="%{$fg_bold[yellow]%}[N]%{$reset_color%}"
	vim_mode=$vim_ins_mode

	function zle-keymap-select {
	  if [ "$KEYMAP" = "vicmd" ]; then
		vim_mode="$vim_cmd_mode"
	  elif [[ "$KEYMAP" = "main" || "$KEYMAP" = "viins" ]]; then
		vim_mode="$vim_ins_mode"
	  fi
	  zle reset-prompt
	}
	zle -N zle-keymap-select

	function zle-line-finish {
	  vim_mode="$vim_ins_mode"
	}
	zle -N zle-line-finish

	# Fix a bug when you C-c in CMD mode and you'd be prompted with CMD mode indicator, while in fact you would be in INS mode
	# Fixed by catching SIGINT (C-c), set vim_mode to INS and then repropagate the SIGINT, so if anything else depends on it, we will not break it
	# Thanks Ron! (see comments)
	function TRAPINT() {
	  vim_mode=$vim_ins_mode
	  return $(( 128 + $1 ))
	}
fi

if [ -n "$__GIT_PROMPT_DIR" ]; then			#test if the repo is loaded
	PROMPT='${time} ${user}${host} $(git_super_status) ${pwd} ${vim_mode}'
	ZSH_THEME_GIT_PROMPT_PREFIX="%{$fg[cyan]%}"
	ZSH_THEME_GIT_PROMPT_SUFFIX="%{$reset_color%} "
	ZSH_THEME_GIT_PROMPT_DIRTY="%{$fg[cyan]%} %{$fg[cyan]%}?%{$fg[cyan]%}%{$reset_color%}"
	ZSH_THEME_GIT_PROMPT_CLEAN="%{$fg[cyan]%}"
	#ZSH_THEME_GIT_PROMPT_CACHE="true"		#too slow reflecting changes
else
	PROMPT='${time} ${user}${host} ${pwd} ${vim_mode} '
fi


# elaborate exitcode on the right when >0
#return_code_enabled="%(?..%{$fg[red]%}%? ↵%{$reset_color%})"
#return_code_enabled="%(?..%{$fg[red]%}%? :(%{$reset_color%})"
return_code_disabled=
return_code=$return_code_enabled

RPS1='${return_code}'

function accept-line-or-clear-warning () {
	if [[ -z $BUFFER ]]; then
		time=$time_disabled
		return_code=$return_code_disabled
	else
		time=$time_enabled
		return_code=$return_code_enabled
	fi
	zle accept-line
}
zle -N accept-line-or-clear-warning
bindkey '^M' accept-line-or-clear-warning



#adapted from https://github.com/robbyrussell/oh-my-zsh/blob/83cf8dc16f51babbb0193c5b97e568739c1f40de/themes/dieter.zsh-theme
#https://github.com/robbyrussell/oh-my-zsh/blob/master/LICENSE.txt
