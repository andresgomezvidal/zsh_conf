if [ "$sourced_functions_yd" = true ]; then
	return 0
fi
sourced_functions_yd=true


yd_url_title_f(){
	wget -t 30 "$1" --output-document wget_youtube-dl_url_title
	local title="$(awk -F '<title>' '/<title>/ {print $2}' wget_youtube-dl_url_title | awk -F '</title>' '{print $1}')"
	rm wget_youtube-dl_url_title
	echo "$title"
}
