#!/usr/bin/env bash

if [ -z "$1" ]; then
	echo "At least one argument is needed. The options are:"
	awk '$0!~/awk/ && ($0~/case \$i in/ || $0~/;;/) {getline; if($0!~/*)/ && $0!~/esac/) {gsub(")","",$0); print $0}}' "$0"
	exit 1
fi


echo "Executing '$0 $@' as user '$USER'"
echo ""



if ! [ -s "functions_min.sh" ]; then
	fmURL="https://gitlab.com/andresgomezvidal/zsh_conf/raw/master/custom/functions_min.sh"
	wget "$fmURL" --output-document "functions_min.sh" || curl -L -o "$fmURL" "functions_min.sh"
	if ! [ $? -eq 0 ]; then
		echo "There was an error ($?) downloading functions_min.sh"
		echo "Exiting the script..."
		exit 1
	fi
fi

. ./functions_min.sh



if [[ "$USER" != "root" ]] && reply_q_f "Can you use sudo?"; then
	can_sudo=0
else
	can_sudo=1
fi

if [ $can_sudo -eq 0 ]; then
	if ! command_exists_hash_f make; then
		if reply_q_f "Install the package for the make command?"; then
			if command_exists_hash_f apt-get ; then
				sudo apt-get install "build-essential"
			elif command_exists_hash_f pacman ; then
				sudo pacman -S --needed "base-devel"
			elif command_exists_hash_f dnf;  then
				sudo dnf install "make"
			elif command_exists_hash_f yum;  then
				sudo yum install "make"
			fi
		fi
	fi
	if ! command_exists_hash_f bash; then					#just in case
		if reply_q_f "Install bash?"; then
			install_try_f 'bash'
			"$0" "$@"
			exit 0
		fi
	fi
	if ! command_exists_hash_f git; then
		if reply_q_f "Install git?"; then
			install_try_f 'git' || ERRORS="$ERRORS\nInstallation of git"
		fi
	fi
	if ! command_exists_hash_f bc; then 					#for version comparations
		if reply_q_f "Install bc?"; then
			install_try_f 'bc' || ERRORS="$ERRORS\nInstallation of bc"
		fi
	fi
fi

if [ -d /data ]; then
	dest=/data
else
	dest=~
fi

echo "Destination path: where to install the files. Eg: With ~ then they are installed in ~/foo"
echo ""
echo -n "Use '$dest' as destination path: [Y/n]"
read REPLY
echo ""		# (optional) move to a new line
if [[ $REPLY =~ ^[Nn]$ ]]; then
	echo -n "Please insert the path to use: "
	read REPLY
	echo ""		# (optional) move to a new line
	dest="$REPLY"
fi
echo -e "Using '$dest' as destination path\n\n"

p1="$dest"
n1=Shell
d1="$p1/$n1"
d1m="$d1/manager"
n1a=antigen
d1a="$d1m/$n1a"
n1z=zgen
d1z="$d1m/$n1z"
d1b="$d1/load"
d1c="$p1/zsh_conf"
r1=https://gitlab.com/andresgomezvidal/zsh_conf
r1a=https://github.com/zsh-users/antigen
r1z=https://github.com/tarjoilija/zgen

n2=autokey
p2="$dest"/.config
d2="$p2/$n2"
d2b="$d2"_scripts
r2=https://gitlab.com/andresgomezvidal/autokey_scripts.git


n4=dotfiles
p4="$dest"
d4="$p4/$n4"

for i in "$@"
do
	case $i in
	-s|-S|shell|Shell)
		cd "$p1" || exit 1
		dir_check_f "$d1" "false"
		git_clone_download_f "$r1" "$d1" "$d1c" || exit 2

		mkdir -p "$d1b"
		mkdir -p "$d1m"/.stack-work

		cd "$d1m" || exit 3

		if [ $can_sudo -eq 0 ]; then
			if ! command_exists_hash_f zsh; then
				install_try_f 'zsh' || ERRORS="$ERRORS\nInstallation of zsh"
			fi
			if [[ ! "$(grep $USER /etc/passwd |cut -d ':' -f7)" =~ 'zsh' ]]; then
				if reply_q_f "Do you want to change your shell to zsh? (Needs logout or even reboot after!!)"; then
					sudo sed -i "s/$USER:\(.*\):.*/$USER:\1:\/usr\/bin\/zsh/g" /etc/passwd
				fi
			fi
			if ! command_exists_hash_f tmux; then
				if reply_q_f "Install tmux?"; then
					install_try_f 'tmux' || ERRORS="$ERRORS\nInstallation of tmux"
				fi
			fi
		fi

		if ! command_exists_hash_f xclip && reply_q_f "Install xclip?"; then
				install_try_f 'xclip'
		fi
		if ! command_exists_hash_f xsel && reply_q_f "Install xsel?"; then
				install_try_f 'xsel'
		fi

		echo "You can download several managers for loading zsh plugins and functionality"
		if reply_q_f "Install the plugin manager ZGEN (recommended)?"; then
			git_clone_download_f "$r1z" "$d1z"
		fi
		if reply_q_f "Install the plugin manager ANTIGEN (not recommended)?" "false by default"; then
			git_clone_download_f "$r1a" "$d1a"
		fi

		if reply_q_f "Install productivity booster FASD?"; then
			git_clone_download_f https://github.com/clvv/fasd temp_fasd
			cd temp_fasd &&
			{
				{
					if [ $can_sudo -eq 0 ] && reply_q_f "Install it globally?"; then
						sudo \make install
					else
						PREFIX=$HOME \make install
					fi
				} || ERRORS="$ERRORS\nInstallation of fasd"

				cd "$d1m" &&
				rm -rf temp_fasd
			}
		fi
		if reply_q_f "Install AUTOENV?" "false by default"; then
			{
				if ! command_exists_hash_f pip; then
					if [ $can_sudo -eq 0 ]; then
						install_try_f pip
					fi
				fi
				if [ $can_sudo -eq 0 ] && reply_q_f "Install it globally?"; then
					sudo pip install autoenv
				else
					pip install --user autoenv
				fi
			} || ERRORS="$ERRORS\nInstallation of autoenv"
		fi
		if reply_q_f "Install tmuxinator?"; then
			{
				if ! command_exists_hash_f cmake; then
					if [ $can_sudo -eq 0 ]; then
						install_try_f cmake
					fi
				fi
				if ! command_exists_hash_f gem; then
					if [ $can_sudo -eq 0 ]; then
						install_try_f gem
					fi
				fi
				if [ $can_sudo -eq 0 ] && reply_q_f "Install it globally?"; then
					sudo gem install tmuxinator --no-user-install
				else
					gem install tmuxinator --user-install
				fi
			} || ERRORS="$ERRORS\nInstallation of tmuxinator"
		fi
		if reply_q_f "Install tmux plugin manager TMP (locally)?"; then
			git_clone_download_f https://github.com/tmux-plugins/tpm ~/.tmux/plugins/tpm
		fi
		if [ $can_sudo -eq 0 ] && reply_q_f "Install globally tmux plugin for mem, cpu and load?"; then
			git_clone_download_f https://github.com/thewtex/tmux-mem-cpu-load temp_tmplugin
			cd temp_tmplugin &&
			{
				{
					if ! command_exists_hash_f cmake; then
						install_try_f cmake
					fi
					\cmake .  &&
					\make &&
					sudo \make install
				} || ERRORS="$ERRORS\nInstallation of tmux-cpu-load plugin"

				cd "$d1m" &&
				rm -rf temp_tmplugin
			}
		fi

		mkdir -p "$d1a"/repos/https-COLON--SLASH--SLASH-github.com-SLASH-olivierverdier-SLASH-zsh-git-prompt.git/
		mkdir -p "$d1z"/olivierverdier/zsh-git-prompt-master
		ln -si "$d1m"/.stack-work "$d1a"/repos/https-COLON--SLASH--SLASH-github.com-SLASH-olivierverdier-SLASH-zsh-git-prompt.git/.stack-work
		ln -si "$d1m"/.stack-work "$d1z"/olivierverdier/zsh-git-prompt-master/.stack-work

		mv_if_exists_f ~/.bashrc ~/.bashrc_pre_zsh_conf_$(date "+%Y_%m_%d-%H.%M.%S")
		mv_if_exists_f ~/.zshrc ~/.zshrc_pre_zsh_conf_$(date "+%Y_%m_%d-%H.%M.%S")

		echo -n "Copy, soft link or nothing for conf files? (can be changed later, there are even alias for it): [C/l/n]"
		read REPLY
		echo ""		# (optional) move to a new line
		#wildcards and loop not working in a virtual machine environment (too slow?), so I add all one by one
		if [[ $REPLY = [Nn] ]]; then
			echo "Nothing option selected"
		elif [[ $REPLY =~ ^[Ll]$ ]]; then
			echo "Link option selected"
			ln -si "$d1/custom/aliases.sh" "$d1b"
			ln -si "$d1/custom/ctheme.zsh" "$d1b"
			ln -si "$d1/custom/personal.sh" "$d1b"
			ln -si "$d1/custom/special_aliases.zsh" "$d1b"
			ln -si "$d1/custom/zzz_common_interactive.sh" "$d1b"
			ln -si "$d1/custom/tmux" "$d1b"
			ln -si "$d1/.bashrc" ~
			ln -si "$d1/.zshrc" ~
			ln -si "$d1/.common_env" ~
			ln -si "$d1/.zshenv" ~
			ln -si "$d1/.inputrc" ~

			if command_exists_hash_f tmux && (( $(echo "$(tmux -V|cut -d ' ' -f2) > 2.4" |bc -l) )); then
				ln -si "$d1/.tmux.conf_after_2.4" ~/.tmux.conf
			elif command_exists_hash_f tmux && (( $(echo "$(tmux -V|cut -d ' ' -f2) <= 2.4" |bc -l) )); then
				ln -si "$d1/.tmux.conf_2.4" ~/.tmux.conf
			else
				ln -si "$d1/.tmux.conf" ~/.tmux.conf
			fi

			#I don't want to slink this, because is supposed to be changed
			cp -i "$d1/.shell_options_example" ~/.shell_options
		else
			echo "Copy option selected"
			cp -i "$d1/custom/aliases.sh" "$d1b"
			cp -i "$d1/custom/ctheme.zsh" "$d1b"
			cp -i "$d1/custom/personal.sh" "$d1b"
			cp -i "$d1/custom/special_aliases.zsh" "$d1b"
			cp -i "$d1/custom/zzz_common_interactive.sh" "$d1b"
			cp -i "$d1/custom/tmux" "$d1b"
			cp -i "$d1/.bashrc" ~
			cp -i "$d1/.zshrc" ~
			cp -i "$d1/.common_env" ~
			cp -i "$d1/.zshenv" ~
			cp -i "$d1/.inputrc" ~
			cp -i "$d1/.shell_options_example" ~/.shell_options

			if command_exists_hash_f tmux && (( $(echo "$(tmux -V|cut -d ' ' -f2) > 2.4" |bc -l) )); then
				cp -i "$d1/.tmux.conf_after_2.4" ~/.tmux.conf
			elif command_exists_hash_f tmux && (( $(echo "$(tmux -V|cut -d ' ' -f2) <= 2.4" |bc -l) )); then
				cp -i "$d1/.tmux.conf_2.4" ~/.tmux.conf
			else
				cp -i "$d1/.tmux.conf" ~/.tmux.conf
			fi
		fi

		if reply_q_f "Do you want to enable some options (you are asked for each one before enabling it)?"; then
			if reply_q_f "Load tmux with the shell (commenting 'tmux_lock=true' in ~/.shell_options)?"; then
				sed -i "s/tmux_lock=true/#tmux_lock=true/g" ~/.shell_options
			fi
			if reply_q_f "Load fasd with the shell (commenting 'fasd_lock=true' in ~/.shell_options)?"; then
				sed -i "s/fasd_lock=true/#fasd_lock=true/g" ~/.shell_options
			fi
			if reply_q_f "Enable vi keybinding in the shell (uncommenting 'keybinding_mode=vi' in ~/.shell_options)?" "false"; then
				sed -i "s/#keybinding_mode/keybinding_mode/g" ~/.shell_options
			fi
		fi

		if [ -n "$ERRORS" ]; then
			echo -e "There were the following ERRORS: \033[0;31m $ERRORS \033[0m"
		fi

	shift # past argument=value
	;;
	-a|-A|autokey|Autokey)
		if ! [ -d "$d2/.git" ]; then
			if [[ -d "$n2" && ! "$PWD" =~ "$p2" ]]; then
				echo -n "There is a '$n2' directory in this folder. Do you want to copy it to '$d2'?: [Y/n]"
				read REPLY
				echo ""		# (optional) move to a new line
				if ! [[ $REPLY =~ ^[Nn]$ ]]; then
					rsync -avhzL "$n2" "$p2"
				fi
			else
				cd "$p2"
				git_clone_download_f "$r2" "$d2"
			fi
		else
			echo "Destination '$d2' has a .git directory!"
		fi
	shift
	;;
	-d|-D|dotfiles|Dotfiles)
		if ! [ -d "$d4/.git" ]; then
			if [[ -d "$n4" && ! "$PWD" =~ "$p4" ]]; then
				echo -n "There is a '$n4' directory in this folder. Do you want to copy it to '$d4'?: [Y/n]"
				read REPLY
				echo ""		# (optional) move to a new line
				if ! [[ $REPLY =~ ^[Nn]$ ]]; then
					rsync -avhzL "$n4" "$p4"
				fi
			else
				cd "$p4"
				git_clone_download_f "https://gitlab.com/andresgomezvidal/dotfiles.git" "dotfiles" && cd dotfiles && ./install.sh
			fi
		else
			echo "Destination '$d4' has a .git directory!"
		fi
	shift
	;;
	-m|-M|minimal|Minimal)
		mv_if_exists_f ~/.zshrc ~/.zshrc_pre_zsh_conf_$(date "+%Y_%m_%d-%H.%M.%S")
		mv_if_exists_f ~/.vimrc ~/.vimrc_pre_zsh_conf_$(date "+%Y_%m_%d-%H.%M.%S")
		mv_if_exists_f ~/.vimrc_common ~/.vimrc_pre_zsh_conf_$(date "+%Y_%m_%d-%H.%M.%S")

		cd "$p1" || exit 1
		if ! command_exists_hash_f zsh && reply_q_f "Install and use zsh?"; then
			{
				install_try_f 'zsh' &&
				{
					git_clone_download_f "$r1z" "$d1z" 				#Zgen
					download_file_f "$r1/raw/master/.zshrc_min" && mv .zshrc_min .zshrc
					if [[ ! "$(grep $USER /etc/passwd |cut -d ':' -f7)" =~ 'zsh' ]]; then
						sudo sed -i "s/$USER:\(.*\):.*/$USER:\1:\/usr\/bin\/zsh/g" /etc/passwd
					fi
				}
			} || ERRORS="$ERRORS\nInstallation of zsh"
		fi
		if ! command_exists_hash_f fasd && reply_q_f "Install productivity booster FASD?"; then
			git_clone_download_f https://github.com/clvv/fasd temp_fasd
			cd temp_fasd &&
			{
				{
					if reply_q_f "Install it globally?"; then
						sudo \make install
					else
						PREFIX=$HOME \make install
					fi
				} || ERRORS="$ERRORS\nInstallation of fasd"

				cd "$p1" &&
				rm -rf temp_fasd
			}
		fi
		cd "$p1" || exit 1
		if ! command_exists_hash_f tmux && reply_q_f "Install tmux?"; then
			{
				install_try_f 'tmux'
				git_clone_download_f https://github.com/tmux-plugins/tpm ~/.tmux/plugins/tpm

				git_clone_download_f https://github.com/thewtex/tmux-mem-cpu-load temp_tmplugin &&
				cd temp_tmplugin &&
				{
					{
						if ! command_exists_hash_f cmake; then
							install_try_f cmake
						fi
						\cmake .  &&
						\make &&
						sudo \make install
					} || ERRORS="$ERRORS\nInstallation of tmux-cpu-load plugin"

					cd "$p1" &&
					rm -rf temp_tmplugin
				}

				if (( $(echo "$(tmux -V|cut -d ' ' -f2) > 2.4" |bc -l) )); then
					download_file_f "$r1/raw/master/.tmux.conf_after_2.4" && mv ".tmux.conf_after_2.4" ~/.tmux.conf
				elif (( $(echo "$(tmux -V|cut -d ' ' -f2) <= 2.4" |bc -l) )); then
					download_file_f "$r1/raw/master/.tmux.conf_2.4" && mv ".tmux.conf_2.4" ~/.tmux.conf
				else
					download_file_f "$r1/raw/master/.tmux.conf" && mv .tmux.conf ~/.tmux.conf
				fi
			} || ERRORS="$ERRORS\nInstallation of tmux"
		fi

		if ! command_exists_hash_f xclip && reply_q_f "Install xclip?"; then
				install_try_f 'xclip'
		fi
		if ! command_exists_hash_f xsel && reply_q_f "Install xsel?"; then
				install_try_f 'xsel'
		fi

		cd "$p1" || exit 1
		if ! command_exists_hash_f vim && reply_q_f "Install vim?"; then
			install_try_f 'vim'
		fi
		download_file_f "https://gitlab.com/andresgomezvidal/dotfiles/raw/master/.vimrc"
		download_file_f "https://gitlab.com/andresgomezvidal/dotfiles/raw/master/.vimrc_common"
		if ! [ -d ~/.vim/bundle/Vundle.vim ]; then
			echo "Installing vundle for vim"
			git_clone_download_f https://github.com/VundleVim/Vundle.vim.git ~/.vim/bundle/Vundle.vim
			echo -n "Do you want to download all vim's plugins now?: [y/N]"
			read REPLY
			echo ""		# (optional) move to a new line
			if [[ $REPLY =~ ^[Yy]$ ]]; then
				vim +PluginUpdate +qall
			fi
		fi

		[[ "$USER" != "root" ]] && if reply_q_f "Copy to root?"; then
			[[ -s /root/.zshrc ]] && sudo mv /root/.zshrc /root/.zshrc_pre_zsh_conf_$(date "+%Y_%m_%d-%H.%M.%S")
			[[ -s /root/.vimrc  ]] && sudo mv /root/.vimrc  /root/.vimrc _pre_zsh_conf_$(date "+%Y_%m_%d-%H.%M.%S")
			[[ -s /root/.vimrc_common ]] && sudo mv /root/.vimrc_common /root/.vimrc_common_pre_zsh_conf_$(date "+%Y_%m_%d-%H.%M.%S")
			[[ -d /root/.vim ]] && sudo mv /root/.vim /root/.vim_common_pre_zsh_conf_$(date "+%Y_%m_%d-%H.%M.%S")
			sudo rsync -a ~/.vim /root
			sudo cp ~/.zshrc /root
			sudo cp ~/.vimrc /root
			sudo cp ~/.vimrc_common /root
		fi
	shift
	;;
	*)
	;;
	esac
done
